use actix_web::{ dev::Payload, web, FromRequest, HttpRequest};
use base64::{engine::general_purpose::STANDARD, Engine};
use diesel::{ExpressionMethods, QueryDsl, RunQueryDsl, OptionalExtension};
use futures_util::future::{err, ready, LocalBoxFuture, Ready};
use crate::{errors::ChallengeListError, models::accounts::{Account, AuthenticatedAccount}, schema::accounts, state::ChallengeListState};
use crate::models::accounts::OuterAccount;

fn authenticate(req: HttpRequest) -> Result<AuthenticatedAccount, ChallengeListError> {
    let state = req.app_data::<web::Data<ChallengeListState>>().unwrap();

    if let Some(cookie) = req.cookie("session") {
        let connection = state.pool.get().unwrap();

        return Ok(AuthenticatedAccount::from_session_token(connection, cookie.value().to_string())?)
    }

    if let Some(auth) = req.headers().get("Authorization") {
        let header_parts: Vec<_> = auth.to_str()
            .map_err(|_| ChallengeListError::Unauthorized)?
            .split(' ')
            .collect();

        match &header_parts[..] {
            ["Bearer", token] => {
                // TODO: create api keys, they're currently nonexistent
                return Err(ChallengeListError::Unauthorized)
            },

            ["Basic", credentials] => {
                let decoded_credentials = STANDARD.decode(credentials)
                    .map_err(|_| ())
                    .and_then(|b| String::from_utf8(b).map_err(|_| ()))
                    .map_err(|_| ChallengeListError::Unauthorized)?;

                if let [username, password] = &decoded_credentials.splitn(2, ':').collect::<Vec<_>>()[..] {
                    let mut connection = state.pool.get().unwrap();
                    let account = accounts::table
                        .filter(accounts::username.eq(*username))
                        .first::<Account>(&mut connection)
                        .optional()?;

                    if account.is_none(){
                        return Err(ChallengeListError::Unauthorized);
                    }

                    let account = account.unwrap();

                    if !account.verify_password(password.to_string()) {
                        return Err(ChallengeListError::Unauthorized);
                    }

                    // TODO: get the closest session to the client's location
                    // return new session if there isn't anything that resembles the client

                    // or, perhaps i could jus not make a session at all since
                    // `AuthenticatedAccount`'s session property is an `Option`
                    return Ok(AuthenticatedAccount::from_session(connection, account)?)
                }
            },

            ["Session", token] => {
                let connection = state.pool.get().unwrap();

                return Ok(AuthenticatedAccount::from_session_token(connection, token.to_string())?)

            }

            _ => return Err(ChallengeListError::Unauthorized)
        }
    }

    Err(ChallengeListError::Unauthorized)
}

pub struct Authorization(pub AuthenticatedAccount);

impl FromRequest for Authorization {
    type Error = ChallengeListError;
    type Future = LocalBoxFuture<'static, Result<Authorization, Self::Error>>;

    fn from_request(req: &HttpRequest, _: &mut Payload) -> Self::Future {
        let req = HttpRequest::clone(req);

        Box::pin(async move {
            Ok(Authorization(authenticate(req)?))
        })
    }
}

pub struct SafeAuthorization(pub Option<AuthenticatedAccount>);

impl FromRequest for SafeAuthorization {
    type Error = ChallengeListError;
    type Future = LocalBoxFuture<'static, Result<SafeAuthorization, Self::Error>>;

    fn from_request(req: &HttpRequest, _: &mut Payload) -> Self::Future {
        let req = HttpRequest::clone(req);

        Box::pin(async move {
            let auth = authenticate(req);
            
            if auth.is_err() {
                Ok(SafeAuthorization(None))
            } else {
                Ok(SafeAuthorization(Some(auth?)))
            }
        })
    }
}

impl SafeAuthorization {
    pub fn execute_or<T>(&self, fun: impl FnOnce(OuterAccount) -> Result<T, ChallengeListError>) -> Result<T, ChallengeListError> {
        if self.0.is_none() {
            return Err(ChallengeListError::Unauthorized);
        }
    
        fun(self.0.clone().unwrap().account)
    }
}
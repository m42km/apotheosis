use std::{default, fmt::Debug, intrinsics::size_of, marker::PhantomData, str::FromStr};

use actix_web::{web, FromRequest, HttpRequest, Responder};
use diesel::{r2d2::{ConnectionManager, PooledConnection}, PgConnection};
use essence::Proxyable;
use futures_util::future::LocalBoxFuture;
use serde::{Deserialize, Serialize};
use qstring::QString;
use serde_json::Value;

use crate::{errors::{ChallengeListError, ValidPaginationParameters}, schema::challenges, service::{database, Response}, state::ChallengeListState};

pub struct Pagination<Output, const HARD_LIMIT: usize>
where Output : Serialize {
    pub page: i32,
    pub limit: Option<i32>,
    pub sort: Option<String>,
    pub filters: Vec<(String, String)>,
    pub defaults: Option<PaginationDefaults>,

    _phantom: PhantomData<Output>
}

impl<Output, const HARD_LIMIT: usize> FromRequest for Pagination<Output, HARD_LIMIT>
where Output : Serialize {
    type Error = ChallengeListError;
    type Future = LocalBoxFuture<'static, Result<Pagination<Output, HARD_LIMIT>, Self::Error>>;

    fn from_request(req: &actix_web::HttpRequest, _: &mut actix_web::dev::Payload) -> Self::Future {
        let req = HttpRequest::clone(req);

        Box::pin(async move {
            let query = queryst::parse(req.query_string())
                .map_err(|_| ChallengeListError::BadRequest)?;
            let mut pagination = Pagination {
                page: 1,
                limit: None,
                sort: None,
                filters: vec![],
                defaults: None,
                _phantom: PhantomData
            };

            let page = query.get("page");
            let sort = query.get("sort");
            let limit = query.get("limit");
            let filters = query.get("filter");

            if page.is_some() {
                let page = safe_cast(page.unwrap(), 1);
                
                if page > 0 {
                    pagination.page = page;
                } else {
                    return Err(ChallengeListError::UniqueBadRequest { message: "The provided page number is invalid." })
                }
            }

            if sort.is_some() {
                pagination.sort = safe_optional_cast::<String>(sort.unwrap());
            }

            if limit.is_some() {
                let base_limit: i32= safe_optional_cast(limit.unwrap()).unwrap_or(10);
                
                // usize is 8 bytes
                // hopefully it isn't system dynamic
                if base_limit > (size_of::<[usize; HARD_LIMIT]>() / 8) as i32 || base_limit < 1 {
                    return Err(ChallengeListError::PaginationLimitInvalid {
                        min: 1,
                        max: (size_of::<[usize; HARD_LIMIT]>() / 8)
                    });
                } else {
                    pagination.limit = Some(base_limit.try_into().unwrap());
                }
            }

            if filters.is_some() {
                let filters = filters.unwrap();

                if filters.is_object() {
                    // only account for object filters
                    for (key, val) in filters.as_object().unwrap().iter() {
                        pagination.filters.push((
                            key.clone(),
                            safe_optional_cast(val).unwrap_or("".to_string())
                        ))
                    }
                }
            }


            /*if query.has("page") == true {
                let page = page.unwrap().parse().unwrap_or(0);

                if page >= 1 {
                    pagination.page = page;
                }
            }

            if query.has("sort") == true {
                pagination.sort = Some(sort.unwrap())
            }

            if query.has("limit") == true {
                let base_limit = limit.unwrap().parse().unwrap_or(0);

                if base_limit > size_of::<[usize; HARD_LIMIT]>() {
                    pagination.limit = Some(size_of::<[usize; HARD_LIMIT]>().try_into().unwrap());
                } else {
                    pagination.limit = Some(base_limit.try_into().unwrap());
                }
            }

            for (key, value) in pairs.iter() {
                if key.to_string().starts_with("filter") {
                    let key = key.to_string();

                    pagination.filters.push((format!("{}", &key[..6]).as_str(), value));
                }
            }*/

            Ok(pagination)
        })
    }
}

impl<Output, const HARD_LIMIT: usize> Pagination<Output, HARD_LIMIT>
where Output : Serialize + Debug + Send + Sync + 'static {
    pub async fn paginate(
        &self,
        state: web::Data<ChallengeListState>,
        allowed_sorts: Vec<&'static str>,
        allowed_filters: Vec<&'static str>,
        paginator: impl FnOnce(&mut PooledConnection<ConnectionManager<PgConnection>>, PaginationParameters) -> Result<Vec<Output>, ChallengeListError>
            + Send
            + 'static,
    ) -> impl Responder {
        let sort;
        let limit;
        let mut page = self.page;

        if page < 0 {
            page = 1;
        }

        if self.defaults.is_some() {
            let defaults = self.defaults.clone().unwrap();

            sort = defaults.sort.map_or(None, |str| Some(str.to_string()));
            limit = defaults.limit.map_or(Some(10), |i| if i > 100 { Some(100) } else { Some(i) });
        } else {
            sort = self.sort.clone();
            limit = self.limit.clone();
        }

        let filters = self.filters.clone();

        if sort.is_some() {
            let sort = sort.clone().unwrap(); // clone to not destroy the original value
            if !allowed_sorts.iter().any(|s| *s == sort) {
                return Err(ChallengeListError::PaginationQueryInvalid { valid: ValidPaginationParameters {
                    sort: allowed_sorts,
                    filter: allowed_filters
                } });
            }
        }

        let banned_filters = filters.iter().filter(|f| !allowed_filters.iter().any(|ff| &f.0 == ff)).collect::<Vec<_>>();

        if banned_filters.len() != 0 {
            return Err(ChallengeListError::PaginationQueryInvalid { valid: ValidPaginationParameters {
                sort: allowed_sorts,
                filter: allowed_filters
            } });
        }

        database::execute(state, move |connection| {
            Ok::<Vec<Output>, ChallengeListError>(paginator(connection, PaginationParameters {
                page,
                sort,
                filters,
                limit: limit.unwrap()
            })?)
        })
        .await
        .and_then(|page| {
            Ok(Response {
                status: 200,
                message: None,
                data: Some(page)
            })
        })
    }

    pub fn with_defaults(&mut self, defaults: PaginationDefaults) {
        self.defaults = Some(defaults);
    }
}

pub struct PaginationParameters {
    pub page: i32,
    pub limit: i32,
    pub sort: Option<String>,
    //pub direction: Option<>
    pub filters: Vec<(String, String)>,
}

impl PaginationParameters {
    pub fn page_offset(&self) -> i64 {
        ((self.page - 1) * self.limit).into()
    }
}

#[derive(Clone)]
pub struct PaginationDefaults {
    pub sort: Option<&'static str>,
    pub limit: Option<i32>
}

impl PaginationDefaults {
    pub fn with(sort: Option<&'static str>, limit: Option<i32>) -> PaginationDefaults {
        PaginationDefaults {
            sort,
            limit
        }
    }
}

/*pub struct Pagination<T>
where 
    T : Serialize
{
    pub page: Option<i32>,
    pub sort: Option<String>,
    pub filter: Option<String>,
    pub limit: Option<i32>,

    #[serde(skip)]
    _phantom: PhantomData<T>,
}

impl<T> Pagination<T>
where
    T : Serialize + Send + Debug + 'static,
{
    pub async fn paginate(
        &self,
        state: web::Data<ChallengeListState>,
        filters: Vec<String>,
        sorts: Vec<String>,
        fetcher: impl FnOnce(&mut PooledConnection<ConnectionManager<PgConnection>>, i32, Option<String>, Option<String>, Option<i32>) -> Result<Vec<T>, ChallengeListError>
        + Send
        + 'static,
        defaults: Option<PaginationDefaults>
    ) -> impl Responder {
        let sort;
        let filter;
        let limit;
        let mut page = self.page.unwrap_or(1);

        if page < 0 {
            page = 1;
        }

        if defaults.is_some() {
            let defaults = defaults.unwrap();

            sort = defaults.sort.map_or(None, |str| Some(str));
            filter = defaults.filter.map_or(None, |str| Some(str));
            limit = defaults.limit.map_or(Some(10), |i| if i > 100 { Some(100) } else { Some(i) });
        } else {
            sort = self.sort.clone();
            filter = self.filter.clone();
            limit = self.limit.clone();
        }

        database::execute(state, move |connection| {
            Ok::<Vec<T>, ChallengeListError>(fetcher(connection, page, sort, filter, limit)?)
        })
        .await
        .and_then(|page| {
            Ok(Response {
                status: 200,
                message: None,
                data: Some(page)
            })
        })
    }
}
*/

pub fn safe_cast<T>(val: &Value, default: T) -> T
    where T : FromStr {
    if !val.is_string() {
        return "".parse().unwrap_or(default);
    }

    val.as_str().unwrap().parse().unwrap_or(default)
}
pub fn safe_optional_cast<T>(val: &Value) -> Option<T>
    where T : FromStr {
    if !val.is_string() {
        return None;
    }

    let ret = val.as_str().unwrap().parse();
    
    match ret {
        Ok(x) => Some(x),
        Err(_) => None,
    }
}
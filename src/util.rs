use regex::Regex;
use reqwest;

// add twitch vids/clips and bilibili to this enum in the future
enum VideoType {
    Youtube
}

/* use &str to only reference strings in params
for all methods here so no borrowing/ownership errors occur */
pub fn assert_video(video: &str) -> bool {
    if let Some(_type) = get_video_type(video) {
        // use the youtube oembed "hack"
        let resp = reqwest::blocking::get(format!("https://www.youtube.com/oembed?url={}&format=json", video));

        // nonexistent/privated videos will return 400 and 403 error response codes respectively
        // existing and public/unlisted will return a successful 200 code
        if resp.is_err() {
            false
        } else {
            true
        }
    } else {
        false
    }
}

fn get_video_type(video: &str) -> Option<VideoType> {
    let YOUTUBE_REGEX = Regex::new(r"(?mi)^(?:https?:\/\/)?(?:(?:www\.)?youtube\.com\/(?:(?:v\/)|(?:embed\/|watch(?:\/|\?)){1,2}(?:.*v=)?|.*v=)?|(?:www\.)?youtu\.be\/)([A-Za-z0-9_\-]+)&?.*$").unwrap();

    if YOUTUBE_REGEX.is_match(video) {
        Some(VideoType::Youtube)
    } else {
        None
    }
}
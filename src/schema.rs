// @generated automatically by Diesel CLI.

diesel::table! {
    accounts (id) {
        id -> Varchar,
        #[max_length = 32]
        username -> Varchar,
        #[max_length = 128]
        password_hash -> Varchar,
        created_at -> Int8,
        updated_at -> Int8,
        #[max_length = 2]
        country_code -> Nullable<Varchar>,
        #[max_length = 8]
        subdivision_code -> Nullable<Varchar>,
        flags -> Int4,
    }
}

diesel::table! {
    challenge_creators (challenge_id, player_id) {
        challenge_id -> Varchar,
        player_id -> Varchar,
    }
}

diesel::table! {
    challenges (id) {
        id -> Varchar,
        #[max_length = 64]
        name -> Varchar,
        position -> Int2,
        video -> Text,
        verifier_id -> Varchar,
        publisher_id -> Varchar,
        level_id -> Nullable<Varchar>,
        flags -> Int4,
    }
}

diesel::table! {
    group_assignments (account_id, group_id) {
        account_id -> Varchar,
        group_id -> Varchar,
    }
}

diesel::table! {
    groups (id) {
        id -> Varchar,
        #[max_length = 32]
        name -> Varchar,
        priority -> Int2,
        #[max_length = 6]
        color -> Varchar,
        permissions -> Int4,
        flags -> Int4,
    }
}

diesel::table! {
    players (id) {
        id -> Varchar,
        #[max_length = 24]
        username -> Varchar,
        banned -> Bool,
        account_id -> Nullable<Varchar>,
        flags -> Int4,
    }
}

diesel::table! {
    record_notes (id) {
        id -> Varchar,
        record_id -> Varchar,
        submitter_id -> Varchar,
        submitted_at -> Int8,
        updated_at -> Int8,
        content -> Varchar,
        public -> Bool,
    }
}

diesel::table! {
    records (id) {
        id -> Varchar,
        challenge_id -> Varchar,
        player_id -> Varchar,
        submitter_id -> Varchar,
        submitted_at -> Int8,
        updated_at -> Int8,
        video -> Text,
        status -> Int2,
        #[sql_name = "type"]
        type_ -> Int2,
    }
}

diesel::table! {
    sessions (id) {
        id -> Varchar,
        account_id -> Varchar,
        token -> Varchar,
        created_at -> Int8,
        expires_at -> Int8,
    }
}

diesel::joinable!(challenge_creators -> challenges (challenge_id));
diesel::joinable!(challenge_creators -> players (player_id));
diesel::joinable!(group_assignments -> accounts (account_id));
diesel::joinable!(group_assignments -> groups (group_id));
diesel::joinable!(record_notes -> accounts (submitter_id));
diesel::joinable!(records -> challenges (challenge_id));
diesel::joinable!(records -> players (player_id));

diesel::allow_tables_to_appear_in_same_query!(
    accounts,
    challenge_creators,
    challenges,
    group_assignments,
    groups,
    players,
    record_notes,
    records,
    sessions,
);

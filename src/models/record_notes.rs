use chrono::Utc;
use crate::schema::{self, record_notes};
use diesel::{AsChangeset, Insertable, PgConnection, QueryDsl, RunQueryDsl};
use uuid::Uuid;
use crate::models::accounts::Account;
use crate::models::records::Record;

essence::model! {
    #[table = schema::record_notes]
    #[derive(AsChangeset)]
    RecordNote {
        id -> String,
        record_id -> String,
        submitter_id -> String,
        submitted_at -> i64,
        updated_at -> i64,
        content -> String,
        public -> bool
    }
}

#[derive(Insertable)]
#[diesel(table_name = schema::record_notes)]
pub struct InsertableRecordNote<'a> {
    pub id: &'a str,
    pub record_id: &'a str,
    pub submitter_id: &'a str,
    pub submitted_at: &'a i64,
    pub updated_at: &'a i64,
    pub content: &'a str,
    pub public: &'a bool
}

impl RecordNote {
    pub fn create(connection: &mut PgConnection, submitter: Account, record: Record, content: String, public: bool) -> diesel::QueryResult<RecordNote> {
        let id = format!("{}", Uuid::new_v4());
        let now = Utc::now();

        diesel::insert_into(record_notes::table)
            .values(InsertableRecordNote {
                id: &id,
                record_id: &record.id,
                submitter_id: &submitter.id,
                submitted_at: &now.timestamp(),
                updated_at: &now.timestamp(),
                content: &content,
                public: &public
            })
            .execute(connection)?;

        record_notes::table
               .find(&id)
               .first::<RecordNote>(connection)
    }
}
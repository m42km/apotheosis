use crate::{
    models::{
        challenges::Challenge,
        players::Player,
        accounts::Account
    },
    schema::{self, players, challenges, records, accounts}
};
use chrono::Utc;
use diesel::{Insertable, PgConnection, RunQueryDsl, QueryDsl, ExpressionMethods, AsChangeset};
use serde::Serialize;
use uuid::Uuid;

essence::model! {
    #[table = schema::records]
    #[diesel(belongs_to(Challenge))]
    #[derive(AsChangeset)]
    Record {
        id -> String,
        #[ignored]
        challenge_id -> String,
        #[ignored]
        player_id -> String,
        #[ignored]
        submitter_id -> String,
        submitted_at -> i64,
        updated_at -> i64,
        video -> String,
        status -> i16,
        #[serde(rename = "type")]
        type_ -> i16,
        
        #[relation(
            typ = OneToOne,
            table = schema::challenges,
            model = Challenge,
            column = records::challenge_id,
            foreign_column = challenges::id
        )]
        challenge -> Challenge,

        #[relation(
            typ = OneToOne,
            table = schema::players,
            model = Player,
            column = records::player_id,
            foreign_column = players::id
        )]
        player -> Player,

        #[ignored]
        #[relation(
            typ = OneToOne,
            table = schema::accounts,
            model = Account,
            column = records::submitter_id,
            foreign_column = accounts::id
        )]
        submitter -> Account,
    }
}

#[derive(Insertable)]
#[diesel(table_name = records)]
pub struct InsertableRecord<'a> {
    pub id: &'a str,
    pub video: &'a str,
    pub challenge_id: &'a str,
    pub player_id: &'a str,
    pub submitter_id: &'a str,
    pub submitted_at: &'a i64,
    pub updated_at: &'a i64,
    pub status: &'a i16,
    pub type_: &'a i16,
}

#[derive(PartialEq)]
#[repr(i16)]
pub enum RecordStatus {
    Submitted,
    UnderConsideration,
    Rejected,
    Approved,
    Unknown
}

impl From<RecordStatus> for i16 {
    fn from(item: RecordStatus) -> i16 {
        match item {
            RecordStatus::Submitted => 0,
            RecordStatus::UnderConsideration => 1,
            RecordStatus::Rejected => 2,
            RecordStatus::Approved => 3,
            RecordStatus::Unknown => -1
        }
    }
}

impl From<String> for RecordStatus {
    fn from(string: String) -> RecordStatus {
        match string.to_lowercase().as_str() {
            "submitted" | "0" => RecordStatus::Submitted,
            "under_consideration" | "1" => RecordStatus::UnderConsideration,
            "rejected" | "2" => RecordStatus::Rejected,
            "approved" | "3" => RecordStatus::Approved,
            _ => RecordStatus::Unknown
        }
    }
}

#[repr(i16)]
pub enum RecordType {
    Completion,
    Verification
}

impl From<RecordType> for i16 {
    fn from(item: RecordType) -> i16 {
        match item {
            RecordType::Completion => 0,
            RecordType::Verification => 1
        }
    }
}

pub struct RecordData {
    pub player: Player,
    pub typ: RecordType,
    pub status: Option<RecordStatus>,
    pub video: String,
    pub raw_footage: Option<String>
}

impl Record {
    pub fn create(connection: &mut PgConnection, submitter: Account, challenge: Challenge, data: RecordData) -> diesel::QueryResult<Record> {
        let id = format!("{}", Uuid::new_v4());
        let now = Utc::now();

        diesel::insert_into(records::table)
            .values(InsertableRecord {
                id: &id,
                challenge_id: &challenge.id,
                player_id: &data.player.id,
                submitter_id: &submitter.id,
                submitted_at: &now.timestamp(),
                video: &data.video,
                updated_at: &now.timestamp(),
                status: &data.status.unwrap_or(RecordStatus::Submitted).try_into().unwrap(),
                type_: &data.typ.try_into().unwrap()
            })
            .execute(connection)?;

        records::table
            .filter(records::id.eq(id))
            .first::<Record>(connection)
    }
}

#[derive(Debug, Serialize)]
pub struct OuterRecordWithSubmitter {
    #[serde(flatten)]
    pub base: OuterRecord,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub submitter: Option<Account>
}

impl IsRecord for OuterRecordWithSubmitter {}

impl OuterRecord {
    pub fn with_submitter_unignored(&self) -> OuterRecordWithSubmitter {
        OuterRecordWithSubmitter {
            base: self.clone(),
            submitter: Some(self.submitter.clone())
        }
    }
    pub fn with_submitter_ignored(&self) -> OuterRecordWithSubmitter {
        OuterRecordWithSubmitter {
            base: self.clone(),
            submitter: None
        }
    }
}
use bitflags::bitflags;
use crate::{
    errors::ChallengeListError, models::players::Player, schema::{self, challenge_creators, challenges, players, records}
};
use chrono::Utc;
use diesel::{query_builder::AsChangeset, r2d2::{ConnectionManager, PooledConnection}, Associations, ExpressionMethods, Identifiable, Insertable, PgConnection, QueryDsl, Queryable, RunQueryDsl, Selectable, QueryableByName};
use essence::{OuterRelation};
use uuid::Uuid;

use super::records::{Record, OuterRecord, RecordStatus};

essence::model! {
    #[table = schema::challenges]
    #[derive(AsChangeset, QueryableByName)]
    #[diesel(check_for_backend(diesel::pg::Pg))]
    Challenge {
        id -> String,
        name -> String,
        position -> i16,
        video -> String,
        #[ignored]
        verifier_id -> String,
        #[ignored]
        publisher_id -> String,
        level_id -> Option<String>,

        #[relation(
            typ = OneToOne,
            table = schema::players,
            model = Player,
            column = challenges::verifier_id,
            foreign_column = players::id
        )]
        verifier -> Option<Player>,
        
        #[relation(
            typ = OneToOne,
            table = schema::players,
            model = Player,
            column = challenges::publisher_id,
            foreign_column = players::id
        )]
        publisher -> Option<Player>,
        
        #[relation(
            typ = ManyToMany,
            table = schema::players,
            model = Player,
            join_model = ChallengeCreator
        )]
        creators -> Vec<Player>,
        flags -> i32,
    }
}

bitflags! {
    #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
    struct ChallengeFlags: i32 {
        const UNLISTED = 1 << 0;
        const SUBMISSION_BANNED = 1 << 1;
    }
}

#[derive(Insertable)]
#[diesel(table_name = schema::challenges)]
pub struct InsertableChallenge<'a> {
    pub id: &'a str,
    pub name: &'a str,
    pub position: &'a i16,
    pub video: &'a str,
    pub verifier_id: &'a str,
    pub publisher_id: &'a str,
    pub level_id: Option<&'a str>
}

#[derive(Identifiable, Selectable, Queryable, Associations, Debug)]
#[diesel(belongs_to(Challenge))]
#[diesel(belongs_to(Player))]
#[diesel(table_name = challenge_creators)]
#[diesel(primary_key(challenge_id, player_id))]
pub struct ChallengeCreator {
    pub challenge_id: String,
    pub player_id: String,
}

#[derive(Debug, serde::Serialize)]
pub struct OuterChallengeWithRecords {
    #[serde(flatten)]
    pub base: OuterChallenge,
    pub records: Vec<OuterRecord>
}

impl Challenge {
    pub fn create(connection: &mut PgConnection, challenge: InsertableChallenge) -> diesel::QueryResult<Challenge>  {
        let id = challenge.id;
        let _now = Utc::now(); // todo: log challenge insertions

        diesel::insert_into(challenges::table)
            .values(challenge)
            .execute(connection)?;

        challenges::table
            .filter(challenges::id.eq(id))
            .first::<Challenge>(connection)
    }
}

impl OuterChallenge {
    pub fn with_records(&self, connection: &mut PooledConnection<ConnectionManager<PgConnection>>) -> Result<OuterChallengeWithRecords, ChallengeListError> {
        let records = records::table
            .filter(records::challenge_id.eq(self.base.id.clone()))
            .filter(records::status.eq(i16::from(RecordStatus::Approved)))
            .load::<Record>(connection)?;

        Ok(OuterChallengeWithRecords {
            base: self.clone(),
            records: records.iter().map(|r| r.serialize(connection)).collect()
        })
    }
}
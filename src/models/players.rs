use bitflags::bitflags;
use crate::schema::{self, accounts, players, records};
use super::{
    accounts::Account,
    challenges::Challenge
};
use diesel::{Associations, ExpressionMethods, Insertable, PgConnection, RunQueryDsl, QueryDsl, SelectableHelper, OptionalExtension};
use diesel::r2d2::{ConnectionManager, PooledConnection};
use serde::Serialize;
use uuid::Uuid;
use crate::errors::ChallengeListError;
use crate::models::records::{Record, RecordStatus, RecordType};

essence::model! {
    #[table = schema::players]
    #[diesel(belongs_to(Account))]
    Player {
        id -> String,
        username -> String,
        banned -> bool,
        #[ignored]
        account_id -> Option<String>,
        flags -> i32,
    }
}

#[derive(Serialize, Debug)]
pub struct PlayerProfile {
    #[serde(flatten)]
    pub base: Player,
    pub completions: Vec<Record>,
    pub verifications: Vec<Record>
}

bitflags! {
    #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
    struct PlayerFlags: i32 {
        const SUBMISSION_BANNED = 1 << 0;
    }
}

#[derive(Insertable)]
#[diesel(table_name = schema::players)]
pub struct InsertablePlayer<'a> {
    pub id: String, // i can use this???????????
    pub username: &'a str,
    pub banned: &'a bool,
    pub account_id: Option<&'a str>,
    pub flags: &'a i32
}

impl Player {
    pub fn create_many(connection: &mut PgConnection, names: Vec<String>) -> diesel::QueryResult<Vec<Player>> {
        let mut ids = vec![];
        let players = names.iter().map(|p| {
            let id = format!("{}", Uuid::new_v4());
            ids.insert(ids.len(), id.clone());

            InsertablePlayer {
                id: id,
                username: p.as_str(),
                banned: &false,
                account_id: None,
                flags: &0
            }
        }).collect::<Vec<InsertablePlayer>>();

        diesel::insert_into(players::table)
            .values(&players)
            .execute(connection)?;

        players::table
            .filter(players::id.eq_any(ids))
            .load::<Player>(connection)
    }

    pub fn to_profile(&self, connection: &mut PgConnection) -> Result<PlayerProfile, ChallengeListError> {
        let account = accounts::table.select(Account::as_select())
            .find(self.clone().account_id.unwrap_or("-1".to_string()))
            .execute(connection)
            .optional()?;
        
        Ok(PlayerProfile {
            base: self.clone(),
            completions: get_records(connection, &self.id, RecordType::Completion),
            verifications: get_records(connection, &self.id, RecordType::Verification)
        })
    }
}


fn get_records(connection: &mut PgConnection, player_id: &str, typ: RecordType) -> Vec<Record> {
    records::table
        .filter(records::player_id.eq(player_id))
        .filter(records::status.eq(i16::from(RecordStatus::Approved)))
        .filter(records::type_.eq(i16::from(typ)))
        .load::<Record>(connection)
        .unwrap()
}

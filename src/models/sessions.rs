use diesel::Insertable;

use crate::{models::accounts::{Account, OuterAccount}, schema::{self, accounts, sessions}};

essence::model! {
    #[table = schema::sessions]
    Session {
        id -> String,
        #[ignored]
        account_id -> String,
        token -> String,
        created_at -> i64,
        expires_at -> i64,

        #[relation(
            typ = OneToOne,
            serialize = Deep,
            table = schema::accounts,
            model = Account,
            column = sessions::account_id,
            foreign_column = accounts::id
        )]
        account -> OuterAccount,
    }
}

#[derive(Insertable)]
#[diesel(table_name = schema::sessions)]
pub struct InsertableSession<'a> {
    pub id: &'a str,
    pub account_id: &'a str,
    pub token: &'a str,
    pub created_at: &'a i64,
    pub expires_at: &'a i64,
}
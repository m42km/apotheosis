use crate::{
    errors::ChallengeListError, models::{groups::Group, sessions::InsertableSession}, schema::{self, accounts, group_assignments, groups, sessions}
};
use argon2::{Argon2, PasswordHash, PasswordVerifier};
use chrono::{Local, Utc, Duration};
use diesel::{associations::{Associations, Identifiable}, query_dsl::methods::FilterDsl, r2d2::{ConnectionManager, PooledConnection}, ExpressionMethods, Insertable, OptionalExtension, PgConnection, Queryable, RunQueryDsl, Selectable, AsChangeset, QueryableByName};
use uuid::Uuid;
use essence::OuterRelation;
use bitflags::{bitflags, Flags};
use super::{groups::Permissions, sessions::{OuterSession, Session}};

essence::model! {
    #[table = schema::accounts]
    #[derive(AsChangeset, QueryableByName)]
    Account {
        id -> String,
        username -> String,
        #[ignored]
        password_hash -> String,
        created_at -> i64,
        updated_at -> i64,
        country_code -> Option<String>,
        subdivision_code -> Option<String>,
        flags -> i32,

        #[relation(
            typ = ManyToMany,
            table = schema::groups,
            model = Group,
            join_model = AccountGroup,
            query = order(groups::priority.desc())
        )]
        groups -> Vec<Group>,
    }
}

bitflags! {
    #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
    struct AccountFlags: i32 {
        const SUBMISSION_BANNED = 1 << 0;
    }
}

#[derive(Identifiable, Selectable, Queryable, Associations, Debug)]
#[diesel(belongs_to(Account))]
#[diesel(belongs_to(Group))]
#[diesel(table_name = group_assignments)]
#[diesel(primary_key(account_id, group_id))]
pub struct AccountGroup {
    pub account_id: String,
    pub group_id: String,
}

#[derive(Insertable)]
#[diesel(table_name = schema::accounts)]
pub struct InsertableAccount<'a> {
    pub id: &'a str,
    pub username: &'a str,
    pub password_hash: &'a str,
    pub created_at: &'a i64,
    pub updated_at: &'a i64,
    pub country_code: Option<&'a str>,
    pub subdivision_code: Option<&'a str>,
    pub flags: &'a i32
}

#[derive(Clone)]
pub struct AuthenticatedAccount {
    pub session: Option<Session>,
    pub account: OuterAccount
}

impl Account {
    pub fn login(&self, connection: &mut PgConnection) -> diesel::QueryResult<Session> {
        let id = format!("{}", Uuid::new_v4());
        let now = Utc::now();
        let expiry = now + Duration::days(30);

        diesel::insert_into(sessions::table)
            .values(InsertableSession {
                id: &id,
                account_id: &self.id,
                token: &format!("{}", Uuid::new_v4()),
                created_at: &now.timestamp(),
                expires_at: &expiry.timestamp()
            })
            .execute(connection)?;

        sessions::table
            .filter(sessions::id.eq(id))
            .first::<Session>(connection)
    }

    pub fn verify_password(&self, password: String) -> bool {
        Argon2::default().verify_password(password.as_bytes(), &PasswordHash::new(&self.password_hash).unwrap()).is_ok()
    }
    
    pub fn register(connection: &mut PgConnection, username: String, password_hash: String) -> diesel::QueryResult<Account> {
        let id = format!("{}", Uuid::new_v4());
        let now = Utc::now();

        diesel::insert_into(accounts::table)
            .values(InsertableAccount {
                id: &id,
                username: &username,
                password_hash: &password_hash,
                created_at: &now.timestamp(),
                updated_at: &now.timestamp(),
                country_code: None,
                subdivision_code: None,
                flags: &0,
            })
            .execute(connection)
            .expect("An internal database error has occured while creating this account");

        Ok(accounts::table
            .filter(accounts::id.eq(id))
            .first::<Account>(connection)
            .expect("An internal database error has occured while creating this account."))
    }
}

impl OuterAccount {
    pub fn has(&self, permission: Permissions) -> bool {
        let val = self.groups.iter().fold(0, |acc, g| acc | g.permissions);

        if val & Permissions::ADMINISTRATOR.bits() != 0 {
            return true;
        }

        val & permission.bits() != 0
    }

    pub fn has_any(&self, permissions: Vec<Permissions>) -> bool {
        let val = self.groups.iter().fold(0, |acc, g| acc | g.permissions);
        let mut has = false;

        for perm in permissions.iter() {
            has = val & Permissions::ADMINISTRATOR.bits() != 0;

            if has == true {
                break;
            }
        }

        has
    }
    
    #[allow(unused_must_use)]
    pub fn has_or_throw(&self, permission: Permissions) -> Result<(), ChallengeListError> {
        if !self.has(permission) {
            Err(ChallengeListError::Forbidden)
        } else {
            Ok(())
        }
    }
}

impl AuthenticatedAccount {
    
    pub fn from_session_token(mut connection: PooledConnection<ConnectionManager<PgConnection>>, token: String) -> Result<AuthenticatedAccount, ChallengeListError> {
        let session = sessions::table
            .filter(sessions::token.eq(token))
            .first::<Session>(&mut connection)
            .optional()
            .unwrap();

        if session.is_none() {
            Err(ChallengeListError::Unauthorized)
        } else {
            let session = session.unwrap().serialize(&mut connection);

            Ok(AuthenticatedAccount {
                session: Some(session.base),
                account: session.account
            })
        }
    }

    pub fn from_session(mut connection: PooledConnection<ConnectionManager<PgConnection>>, account: Account) -> Result<AuthenticatedAccount, ChallengeListError> {
        let session = account.login(&mut connection)?;

        Ok(AuthenticatedAccount {
            session: Some(session),
            account: account.serialize(&mut connection)
        })
    }
}
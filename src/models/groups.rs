use bitflags::{bitflags, Flags};

use crate::schema::{self, groups};

essence::model! {
    #[table = schema::groups]
    Group {
        id -> String,
        name -> String,
        priority -> i16,
        color -> String,

        #[ignored]
        permissions -> i32,
        flags -> i32
    }
}

bitflags! {
    #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub struct Permissions: i32 {
        const ADMINISTRATOR = 1 << 0;
        const DELETE_ACCOUNTS = 1 << 2;

        // SITE ADMINISTRATOR PERMS
        const MANAGE_ACCOUNTS = 1 << 3;
        const MANAGE_GROUPS = 1 << 4;
        const MANAGE_ACCOUNT_GROUPS = 1 << 5;

        // SITE MODERATOR PERMISSIONS
        const BAN_ACCOUNTS = 1 << 6;

        // LIST OWNER PERMISSIONS
        const DELETE_CHALLENGES = 1 << 7;
        
        // LIST MODERATOR PERMISSIONS
        const MANAGE_CHALLENGES = 1 << 8;
        const MANAGE_PLAYERS = 1 << 9;
        const MANAGE_PACKS = 1 << 10; // Particularist permission
        const MANAGE_BADGES = 1 << 11; // Particularist permission

        // LIST HELPER PERMISSIONS
        const MANAGE_RECORDS = 1 << 12;

        const LIST_OWNER = Self::DELETE_CHALLENGES.bits() | Self::MANAGE_ACCOUNT_GROUPS.bits() | Self::LIST_MODERATOR_SPECIAL.bits();
        const LIST_MODERATOR_SPECIAL = Self::MANAGE_PACKS.bits() | Self::MANAGE_BADGES.bits() | Self::LIST_MODERATOR.bits();
        const LIST_MODERATOR = Self::MANAGE_CHALLENGES.bits() | Self::MANAGE_PLAYERS.bits() | Self::LIST_HELPER.bits();
        const LIST_HELPER = Self::MANAGE_RECORDS.bits();

        const SITE_OWNER = Self::ADMINISTRATOR.bits() | Self::SITE_ADMINISTRATOR.bits();
        const SITE_ADMINISTRATOR = Self::MANAGE_ACCOUNTS.bits() | Self::MANAGE_GROUPS.bits() | Self::SITE_MODERATOR.bits();
        const SITE_MODERATOR = Self::BAN_ACCOUNTS.bits();
    }
}

bitflags! {
    #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub struct GroupFlags: i32 {
        const IS_LIST_HELPER = 1 << 0;
        const IS_LIST_MODERATOR = 1 << 1;
        const IS_LIST_OWNER = 1 << 2;
        const IS_SITE_MODERATOR = 1 << 3;
        const IS_SITE_OWNER = 1 << 4;
    }
}

impl Group {
    pub fn has(&self, permission: Permissions) -> bool {
        if self.permissions & Permissions::ADMINISTRATOR.bits() != 0 {
            return true;
        }

        self.permissions & permission.bits() != 0
    }
}
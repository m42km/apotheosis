use std::error::Error;

use actix_multipart::MultipartError;
use actix_web::{error::{JsonPayloadError, ResponseError}, guard, http::{Method, StatusCode}, HttpResponse};
use derive_more::Display;
use diesel::r2d2;
use serde::{Serialize, Serializer};
use serde_json::json;

use crate::{models::challenges::Challenge, service::Response};

#[derive(Debug, Serialize, Clone)]
pub struct ValidPaginationParameters {
    pub sort: Vec<&'static str>,
    pub filter: Vec<&'static str>
}

#[derive(Debug, Display, Serialize, Clone)]
#[serde(untagged)]
pub enum ChallengeListError {
    //* `4xx REQUEST ERROR` error codes *//
    /// `400 BAD REQUEST
    /// An error that occurs because the client sent a general request that
    /// the application could not understand.
    #[display(fmt = "The application was unable to understand this request.")]
    BadRequest,

    /// `400 BAD REQUEST`
    /// An error that occurs because the client sent a request that the
    /// application could not understand, but with a unique message that
    /// details what happened.
    #[display(fmt = "{message}")]
    UniqueBadRequest {
        #[serde(skip)]
        message: &'static str,
    },

    // ACCOUNTS

    /// `400 BAD REQUEST`
    /// An error that occurs because the client has sent invalid credentials
    /// (e.g incorrect password, bad username)`
    #[display(fmt = "The provided username or password is invalid.")]
    InvalidUsernameOrPassword,
    
    /// `400 BAD REQUEST`
    /// An error that occurs because the client tried to create an account with
    /// a username that is too long, or too short.
    #[display(fmt = "The provided username is too long. It must be between {min} and {max} characters.")]
    InvalidUsernameLength {
        min: usize,
        max: usize
    },

    /// `400 BAD REQUEST`
    /// An error that occurs because the client tried to create an account with
    /// a username that containes non-alphanumeric characters.
    #[display(fmt = "The provided username contains invalid characters. Usernames must be alphanumeric and contain no leading/trailing spaces.")]
    InvalidUsername,

    /// `400 BAD REQUEST`
    /// An error occurs because the client tried to create an account with a
    /// passowrd that is too short.
    #[display(fmt = "The provided password must be at least {min} characters long.")]
    InvalidPasswordLength {
        min: usize
    },
    
    // CHALLENGES

    /// `400 BAD REQUEST`
    /// An error that occurs when the client sends a list type parameter
    /// that isn't valid.
    ListTypeParameterInvalid,

    /// `400 BAD REQUEST`
    /// An error that occurs because the client tried to add a challenge
    /// to a position that's higher than 1, or is greater than the total
    /// list of challenges on the list.
    #[display(fmt = "The provided position must be greater than or equal to 1, or equal to the number of challenges + 1.")]
    PositionTooGreatOrSmall {
        lowest: usize,
        highest: usize
    },
    
    // RECORDS
    
    /// `400 BAD REQUEST`
    /// An error that occurs because the client tried to delete a record
    /// that is marked as a verification record.
    #[display(fmt = "This record cannot be deleted, as it is marked as the verification record for the challenge.")]
    RecordIsVerification,

    // MISC

    /// `400 BAD REQUEST`
    /// An error that occurs because the client tried to use a filter
    /// or a sort parameter that isn't accepted by the endpoint.
    #[display(fmt = "The provided sort and/or filter is invalid, and must be changed.")]
    PaginationQueryInvalid {
        valid: ValidPaginationParameters
    },

    #[display(fmt = "The provided limit must be a number greater than {min}, and smaller than {max}")]    
    PaginationLimitInvalid {
        min: usize,
        max: usize
    },

    /// `401 UNAUTHORIZED`
    /// An error that occurs because the client sent credentials that the
    /// application has deemed invalid.
    #[display(fmt = "The provided authorization key and/or session token is invalid.")]
    Unauthorized,
    
    /// `403 FORBIDDEN`
    /// An error that occurs because the client tried to access a resource
    /// with credentials that don't have the authority to access it.
    #[display(fmt = "You do not have permission to perform this action against this endpoint.")]
    Forbidden,
    
    /// `403 FORBIDDEN`
    /// An error that occurs because the client tried to access a resource
    /// that they don't own, or have access to.
    #[display(fmt = "You do not have permission to perform this action against this {model}, as it either doesnt belong to you, or your credentials don't have the required permissions.")]
    ModelForbidden {
        model: &'static str
    },

    /// `404 NOT FOUND`
    /// The client tried to request an endpoint that doesn't exist.
    #[display(fmt = "The requested endpoint was not found.")]
    NotFound,

    /// `404 NOT FOUND`
    /// An error that occurs because the client tried to access a resource
    /// that doesn't exist.
    #[display(fmt = "No {model} with this identifier could be found.")]
    ModelNotFound {
        #[serde(skip)]
        model: &'static str
    },
    
    /// `404 NOT FOUND`
    /// An error that occurs because the client tried to provide an id
    /// to a model that doesn't exist.
    #[display(fmt = "The referenced {model} for the {parameter} parameter could not be found.")]
    ReferencedModelNotFound {
        #[serde(skip)]
        model: &'static str,
        
        #[serde(skip)]
        parameter: &'static str,
    },
    
    /// `405 METHOD NOT ALLOWED`
    /// An error that occurs because the  client tried to access an endpoint 
    /// with a method that isn't accepted by the endpoint itself.
    #[display(fmt = "This endpoint does not support this request method.")]
    MethodNotAccepted /*{
        // ...how do i implement this?
        allowed: [?]
    }*/,

    /// `409 CONFLICT`
    /// An error that occurs because the client has tried to register an account
    /// with a username that is already taken.
    #[display(fmt = "This username has already been taken.")]
    UsernameAlreadyTaken,

    /// `409 CONFLICT`
    /// An error that occurs because the client tried to add a creator to a challenge
    /// that is already registered as one.
    #[display(fmt = "This player is already registered as a creator for this challenge.")]
    CreatorRegistered,
    
    /// `409 CONFLICT`
    /// An error that occurs because the client tried to remove a creator from a challenge
    /// that is not registered as one.
    #[display(fmt = "This player is not registered as a creator for this challenge.")]
    CreatorNotRegistered,

    /// `409 CONFLICT`
    /// An error that occurs because the client tried to add an account to a group that
    /// it is already a member of.
    #[display(fmt = "This account is already a member of this group.")]
    GroupRegistered,

    /// `409 CONFLICT`
    /// An error that occurs because the client tried to add an account to a group that
    /// it is already a member of.
    #[display(fmt = "This account is not a member of this group.")]
    GroupNotRegistered,

    /// `413 PAYLOAD TOO LARGE`
    /// The client sent a request with a body that's larger than the application or
    /// endpoint can even handle.
    #[display(fmt = "The provided request body is too large.")]
    RequestTooLarge {
        limit: usize
    },

    /// `415 UNSUPPORTED MEDIA TYPE`
    /// An error that occurs when the client sends a content body with a type that 
    /// isnt accepted by the endpoint.
    #[display(fmt = "This endpoint does not accept the provided body under this content type, as it expects a different kind.")]
    UnsupportedContentType {
        expected: &'static str
    },

    //* `5xx SERVER ERROR` error codes *//

    /// `500 INTERNAL SERVER ERROR`
    /// An error that occurs whenever the application has encountered an unrecoverable
    /// error, and it is thus completely impossible to continue the request.
    #[display(fmt = "An internal application error has occurred.")]
    InternalError,

    /// `500 INTERNAL SERVER ERROR`
    /// An error that occurs whenever the database encounters an unrecoverable error,
    /// and nothing can be done to keep the request going.
    #[display(fmt = "An internal database error has occurred.")]
    DatabaseError,

    /// `500 INTERNAL SERVER ERROR`
    /// An error that occurs whenever a connection cannot be made to the database.
    #[display(fmt = "The application was unable to make a connection to the database.")]
    DatabaseConnectionError,

    /// An error that isn't naturally handled by the application's error handler,
    /// acting as a fallback with a generic message and status code.
    #[display(fmt = "An unknown application error has occurred.",)]
    Unknown {
        #[serde(skip)]
        status: StatusCode,
    }
}

impl std::error::Error for ChallengeListError {}

impl ResponseError for ChallengeListError {
    fn status_code(&self) -> actix_web::http::StatusCode {
        match *self {
            // 4xx CLIENT ERROR
            // 400 BAD REQUEST
            ChallengeListError::BadRequest => StatusCode::BAD_REQUEST,
            ChallengeListError::UniqueBadRequest { .. } => StatusCode::BAD_REQUEST,
            ChallengeListError::ListTypeParameterInvalid => StatusCode::BAD_REQUEST,
            ChallengeListError::PositionTooGreatOrSmall { .. } => StatusCode::BAD_REQUEST,
            ChallengeListError::InvalidUsernameLength { .. } => StatusCode::BAD_REQUEST,
            ChallengeListError::InvalidUsername => StatusCode::BAD_REQUEST,
            ChallengeListError::InvalidPasswordLength { .. } => StatusCode::BAD_REQUEST,
            ChallengeListError::PaginationQueryInvalid { .. } => StatusCode::BAD_REQUEST,
            ChallengeListError::PaginationLimitInvalid { .. } => StatusCode::BAD_REQUEST,
            ChallengeListError::RecordIsVerification => StatusCode::BAD_REQUEST,
            
            // 401 UNAUTHORIZED 
            ChallengeListError::Unauthorized => StatusCode::UNAUTHORIZED,
            
            // 403 FORBIDDEN
            ChallengeListError::Forbidden => StatusCode::FORBIDDEN,
            ChallengeListError::ModelForbidden { .. } => StatusCode::FORBIDDEN,

            // 404 NOT FOUND,
            ChallengeListError::NotFound => StatusCode::NOT_FOUND,
            ChallengeListError::ModelNotFound { .. } => StatusCode::NOT_FOUND,

            // 405 METHOD NOT ALLOWED
            ChallengeListError::MethodNotAccepted { .. } => StatusCode::METHOD_NOT_ALLOWED,

            // 409 CONFLICT
            ChallengeListError::UsernameAlreadyTaken => StatusCode::CONFLICT,
            ChallengeListError::CreatorRegistered => StatusCode::CONFLICT,
            ChallengeListError::CreatorNotRegistered => StatusCode::CONFLICT,

            // 415 UNSUPPORTED MEDIA TYPE
            ChallengeListError::UnsupportedContentType { .. } => StatusCode::UNSUPPORTED_MEDIA_TYPE,
            // 5xx SERVER ERROR
            // 500 INTERNAL SERVER ERROR
            ChallengeListError::InternalError => StatusCode::INTERNAL_SERVER_ERROR,
            ChallengeListError::DatabaseError => StatusCode::INTERNAL_SERVER_ERROR,
            ChallengeListError::DatabaseConnectionError => StatusCode::INTERNAL_SERVER_ERROR,

            // UNKNOWN
            ChallengeListError::Unknown { status, .. } => status,
            _ => StatusCode::INTERNAL_SERVER_ERROR
        }
    }

    fn error_response(&self) -> actix_web::HttpResponse<actix_web::body::BoxBody> {
        HttpResponse::build(self.status_code()).json(json!({
            "status": self.status_code().as_u16(),
            "message": self.to_string(),
            "data": self
        }))
    }
}

impl From<JsonPayloadError> for ChallengeListError {
    fn from(error: JsonPayloadError) -> ChallengeListError {
        match error {
            JsonPayloadError::ContentType => ChallengeListError::UnsupportedContentType { expected: "application/json" },
            JsonPayloadError::OverflowKnownLength { length, limit } => ChallengeListError::RequestTooLarge { limit },
            JsonPayloadError::Overflow { limit } => ChallengeListError::RequestTooLarge { limit },
            _ => ChallengeListError::BadRequest
        }
    }
}

impl From<MultipartError> for ChallengeListError {
    fn from(error: MultipartError) -> ChallengeListError {
        match error {
            MultipartError::NoContentType => ChallengeListError::UnsupportedContentType { expected: "multipart/form-data" },
            MultipartError::ParseContentType => ChallengeListError::UnsupportedContentType { expected: "multipart/form-data" },
            _ => ChallengeListError::BadRequest
        }
    }
}

impl From<r2d2::Error> for ChallengeListError {
    fn from(error: r2d2::Error) -> ChallengeListError {
        log::error!("Database error: {:?}", error);

        ChallengeListError::BadRequest
    }
}

impl From<diesel::result::Error> for ChallengeListError {
    fn from(error: diesel::result::Error) -> ChallengeListError {
        panic!("Database error: {}", error);
        match error {
            diesel::result::Error::DatabaseError(_, _) => ChallengeListError::BadRequest,
            diesel::result::Error::NotFound => ChallengeListError::BadRequest,
            _ => ChallengeListError::DatabaseError 
        }
    }
}
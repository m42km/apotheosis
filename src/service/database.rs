use std::{
    error::Error,
    fmt::Debug,
    sync::{Arc, Mutex},
};

use crate::{errors::ChallengeListError, state::ChallengeListState};
use actix_web::web;
use diesel::{
    Connection,
    r2d2::{ConnectionManager, PooledConnection},
    PgConnection,
};

/// A threaded function where database operations are meant to be performed, allowing for
/// managing and returning objects in the application's database, with a means of controlling
/// which application error is sent.
pub async fn execute<F, R, E>(
    state: web::Data<ChallengeListState>,
    f: F,
) -> Result<R, ChallengeListError>
where
    F: FnOnce(&mut PooledConnection<ConnectionManager<PgConnection>>) -> Result<R, E>
        + Send
        + 'static,
    R: Send + Debug + 'static,
    E: Error + Into<ChallengeListError>,
{
    // create a mutex, as `BlockingError` doesn't retain the actual error
    // provided from the block function, requiring us to assign it over
    // the new thread
    let error_mutex = Arc::new(Mutex::new(None));
    let error_mutex_clone = Arc::clone(&error_mutex);

    // diesel & r2d2 create blocking operations, so we have to run this on a
    // seperate thread to not block the main one
    let output = web::block(move || {
        let connection = state.pool.get();

        if connection.is_err() {
            return Err(ChallengeListError::DatabaseConnectionError);
        }

        connection.unwrap().transaction(|conn| {
            let result = f(conn);

            if result.is_err() {
                let err = result.unwrap_err();

                if let Ok(cl_err) = TryInto::<ChallengeListError>::try_into(err) {
                    let mut mutable_error = error_mutex_clone.lock().unwrap();
                    *mutable_error = Some(cl_err);
                }

                return Err(ChallengeListError::InternalError);
            }

            Ok(result.unwrap())
        })
    })
    .await;

    // we're forced to unwrap the possible result here as unwrapping it
    // right after the function block will cause it to panic if there
    // was an error during the closure

    // obtain the error we may have set during the closure
    let error_mutex_clone2 = Arc::clone(&error_mutex);
    let error = error_mutex_clone2.lock().unwrap();
    if error.is_some() {
        return Err(error.clone().unwrap());
    }

    // we don't know why or how this came to be, but it did
    // actix wont tell us with a blocking error anymore
    if output.is_err() {
        return Err(ChallengeListError::InternalError);
    }

    output.unwrap()
}

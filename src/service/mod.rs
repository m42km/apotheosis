use actix_web::{body::BoxBody, error::BlockingError, http::header::ContentType, HttpResponse, Responder};
use serde::Serialize;

pub mod generic;
pub mod database;

fn respond_to_impl<T>(data: &T, _: &actix_web::HttpRequest) -> actix_web::HttpResponse<BoxBody>
where T : Serialize {
    HttpResponse::Ok()
        .content_type(ContentType::json())
        .body(serde_json::to_string(data).unwrap())
}


#[derive(Debug, Serialize)]
pub struct Response<T=()>
where T : Serialize {
    pub status: i32,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub message: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub data: Option<T>
}

impl<T> Responder for Response<T>
where T : Serialize {
    type Body = BoxBody;
    
    fn respond_to(self, req: &actix_web::HttpRequest) -> actix_web::HttpResponse<Self::Body> {
        respond_to_impl::<Self>(&self, req)
    }
}

#[derive(Serialize)]
pub struct ResponseData<T> {
    pub status: i32,
    pub data: T
}

impl<T> Responder for ResponseData<T>
where T : Serialize {
    type Body = BoxBody;
    
    fn respond_to(self, req: &actix_web::HttpRequest) -> actix_web::HttpResponse<Self::Body> {
        respond_to_impl::<Self>(&self, req)
    }
}
use actix_web::{get, Responder, web};
use diesel::{RunQueryDsl, sql_query};
use diesel::sql_types::Integer;
use serde::Serialize;
use crate::errors::ChallengeListError;
use crate::models::accounts::Account;
use crate::models::groups::GroupFlags;
use crate::service::{database, Response};
use crate::state::ChallengeListState;

#[derive(Serialize, Debug)]
struct TeamMembers {
    pub list: Vec<Account>,
    pub site: Vec<Account>
}

#[get("/team")]
pub async fn team(
    state: web::Data<ChallengeListState>,
) -> impl Responder {
    database::execute(state, move |connection| {
        let mut list_staff = sql_query(include_str!("../../sql/query_accounts_by_group_flag.sql"))
            .bind::<Integer, _>(GroupFlags::IS_LIST_HELPER.bits())
            .bind::<Integer, _>(GroupFlags::IS_LIST_HELPER.bits())
            .bind::<Integer, _>(GroupFlags::IS_LIST_HELPER.bits())
            .load::<Account>(connection)?;
        
        let mut site_staff = sql_query(include_str!("../../sql/query_accounts_by_group_flag_2.sql"))
            .bind::<Integer, _>(GroupFlags::IS_SITE_MODERATOR.bits())
            .bind::<Integer, _>(GroupFlags::IS_SITE_OWNER.bits())
            .load::<Account>(connection)?;

        Ok::<Response<TeamMembers>, ChallengeListError>(Response {
            status: 200,
            message: None,
            data: Some(TeamMembers {
                list: list_staff,
                site: site_staff
            })
        })
    }).await
}
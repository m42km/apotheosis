use crate::{
    errors::ChallengeListError,
    models::accounts::{Account, AccountGroup},
    schema::{accounts, groups},
    service::{database, Response},
    state::ChallengeListState,
};
use actix_web::{post, web, Responder};
use diesel::{connection, ExpressionMethods, QueryDsl, RunQueryDsl};
use essence::OuterRelation;
use serde::Deserialize;

#[derive(Deserialize)]
struct AccountDetails {
    pub username: String,
    pub password: String,
}

#[post("/register")]
pub async fn register(
    state: web::Data<ChallengeListState>,
    details: web::Json<AccountDetails>,
) -> impl Responder {
    database::execute(state.clone(), move |connection| {
        if !(2..12).contains(&details.username.len()) {
            return Err(ChallengeListError::InvalidUsernameLength { min: 2, max: 12 });
        }

        if !details.username.chars().all(char::is_alphanumeric) {
            return Err(ChallengeListError::InvalidUsername);
        }

        let existing = accounts::table
            .filter(accounts::username.eq(details.username.to_string()))
            .first::<Account>(connection);

        if existing.is_ok() {
            return Err(ChallengeListError::UsernameAlreadyTaken);
        }

        let acc = Account::register(
            connection,
            details.username.to_string(),
            state.hash_password(details.password.as_bytes()),
        );

        if acc.is_err() {
            return Err(ChallengeListError::from(acc.unwrap_err()));
        }

        Ok(Response {
            status: 200,
            message: Some("Your account has successfully been created.".to_string()),
            data: Some(acc.unwrap().serialize(connection)),
        })
    }).await
}

#[post("/login")]
pub async fn login(
    state: web::Data<ChallengeListState>,
    details: web::Json<AccountDetails>,
) -> impl Responder {
    database::execute(state.clone(), move |connection| {
        let existing = accounts::table
            .filter(accounts::username.eq(details.username.to_string()))
            .first::<Account>(connection);

        if existing.is_err() {
            return Err(ChallengeListError::InvalidUsernameOrPassword);
        }

        let account = existing.unwrap();

        if !account.verify_password(details.password.clone()) {
            return Err(ChallengeListError::InvalidUsernameOrPassword);
        }
        let session = account.login(connection);
        println!("hi");

        if session.is_err() {
            return Err(ChallengeListError::from(session.unwrap_err()));
        }

        Ok(Response {
            status: 200,
            message: None,
            data: Some(session?.serialize(connection)),
        })
    }).await
}

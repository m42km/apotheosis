use actix_web::{HttpRequest, Responder};

use crate::{errors::ChallengeListError, service::Response};

pub mod authorization;
pub mod accounts;
pub mod challenges;
pub mod records;
pub mod record_notes;
pub mod players;
pub mod groups;
pub mod meta;

pub async fn handle_default(request: HttpRequest) -> Result<Response, ChallengeListError> {
    if request.resource_map().has_resource(request.path()) {
        return Err(ChallengeListError::MethodNotAccepted);
    }

    Err(ChallengeListError::NotFound)
}
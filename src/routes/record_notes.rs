use actix_web::{delete, get, patch, put, Responder, web};
use diesel::{OptionalExtension, QueryDsl, RunQueryDsl, ExpressionMethods};
use essence::{Outer, Proxy};
use serde::Deserialize;
use crate::errors::ChallengeListError;
use crate::extractors::sessions::Authorization;
use crate::models::groups::Permissions;
use crate::models::record_notes::RecordNote;
use crate::models::records::Record;
use crate::schema::{record_notes, records};
use crate::service::{database, Response};
use crate::state::ChallengeListState;

#[get("/{id}/notes")]
pub async fn index(
    state: web::Data<ChallengeListState>,
    path: web::Path<(String)>,
    auth: Authorization
) -> impl Responder {
    database::execute(state, move |connection| {
        let (id) = path.into_inner();
        let record = records::table
            .find(id)
            .first::<Record>(connection)
            .optional()?;

        if record.is_none() {
            Err(ChallengeListError::ModelNotFound {
                model: "record"
            })
        } else {
            let record = record.unwrap();
            let base = record_notes::table.filter(record_notes::record_id.eq(record.id));

            if !auth.0.account.has(Permissions::MANAGE_RECORDS) {
                if record.submitter_id != auth.0.account.base.id {
                    return Err(ChallengeListError::ModelForbidden {
                        model: "record"
                    });
                }

                Ok(Response {
                    status: 200,
                    message: None,
                    data: Some(base.filter(record_notes::public.eq(false))
                        .load::<RecordNote>(connection)?)
                })
            } else {
                Ok(Response {
                    status: 200,
                    message: None,
                    data: Some(base.load::<RecordNote>(connection)?)
                })
            }
        }
    }).await
}

#[derive(Deserialize)]
struct RecordNoteData {
    pub content: String,
    pub public: bool
}

#[put("/{id}/notes")]
pub async fn create(
    state: web::Data<ChallengeListState>,
    path: web::Path<(String)>,
    auth: Authorization,
    data: web::Json<RecordNoteData>
) -> impl Responder {
    auth.0
        .account
        .has_or_throw(Permissions::MANAGE_RECORDS)?;

    database::execute(state, move |connection| {
        let (id) = path.into_inner();
        let record = records::table
            .find(id)
            .first::<Record>(connection)
            .optional()?;

        if record.is_none() {
            Err(ChallengeListError::ModelNotFound {
                model: "record"
            })
        } else {
            let record = record.unwrap();

            Ok(Response {
                status: 201,
                message: None,
                data: Some(RecordNote::create(
                    connection,
                    auth.0.account.base,
                    record,
                    data.content.clone(),
                    data.public.clone()
                )?.serialize())
            })
        }
    }).await
}

#[derive(Deserialize)]
struct ModifiableRecordNote {
    pub content: Option<String>,
    pub public: Option<bool>
}

#[patch("/{id}/notes/{note_id}")]
pub async fn update(
    state: web::Data<ChallengeListState>,
    path: web::Path<(String, String)>,
    auth: Authorization,
    data: web::Json<ModifiableRecordNote>
) -> impl Responder {
    auth.0
        .account
        .has_or_throw(Permissions::MANAGE_RECORDS)?;

    database::execute(state, move |connection| {
        let (record_id, note_id) = path.into_inner();
        let record = records::table
            .find(record_id)
            .first::<Record>(connection)
            .optional()?;

        if record.is_none() {
            return Err(ChallengeListError::ModelNotFound {
                model: "record"
            });
        }

        let record_note = record_notes::table
            .find(note_id)
            .first::<RecordNote>(connection)
            .optional()?;

        if record_note.is_none() {
            return Err(ChallengeListError::ModelNotFound {
                model: "record note"
            });
        }

        let record_note = record_note.unwrap();
        let mut proxy = Proxy::from(record_note.clone());

        if record_note.submitter_id != auth.0.account.base.id {
            if (!auth.0.account.has(Permissions::MANAGE_PLAYERS)) {
                // TODO: come up with a permission that's for this kind of thing, i think
                return Err(ChallengeListError::ModelForbidden {
                    model: "record note"
                })
            }
        }

        if data.content.is_some() {
            let content = data.content.clone().unwrap();

            proxy.set("content", content);
        }

        if data.public.is_none() {
            let public = data.public.clone().unwrap();

            proxy.set("public", public);
        }

        if proxy.has_changes() {
            diesel::update(record_notes::table)
                .filter(record_notes::id.eq(&record_note.id))
                .set(proxy.finalize())
                .execute(connection)?;

            Ok(Response {
                status: 200,
                message: None,
                data: Some(
                    record_notes::table
                        .find(&record_note.id)
                        .first::<RecordNote>(connection)?
                        .serialize()
                )
            })
        } else {
            Ok(Response {
                status: 304,
                message: None,
                data: None
            })
        }
    }).await
}

#[delete("/{id}/notes/{note_id}")]
pub async fn delete(
    state: web::Data<ChallengeListState>,
    path: web::Path<(String, String)>,
    auth: Authorization,
) -> impl Responder {
    auth.0
        .account
        .has_or_throw(Permissions::MANAGE_RECORDS)?;

    database::execute(state, move |connection| {
        let (_, note_id) = path.into_inner();
        let record_note = record_notes::table
            .find(note_id)
            .first::<RecordNote>(connection)
            .optional()?;

        if record_note.is_none() {
            return Err(ChallengeListError::ModelNotFound {
                model: "record note"
            });
        }

        let record_note = record_note.unwrap();
        if record_note.submitter_id != auth.0.account.base.id {
            if (!auth.0.account.has(Permissions::MANAGE_PLAYERS)) {
                return Err(ChallengeListError::ModelForbidden {
                    model: "record note"
                })
            }
        }

        diesel::delete(record_notes::table)
            .filter(record_notes::id.eq(&record_note.id))
            .execute(connection);

        Ok(Response {
            status: 200,
            message: None,
            data: Some(record_note)
        })
    }).await
}
use actix_web::{delete, get, patch, put, Responder, web};
use diesel::{OptionalExtension, PgConnection, QueryDsl, RunQueryDsl, SelectableHelper, ExpressionMethods};
use diesel::r2d2::{ConnectionManager, PooledConnection};
use essence::{OuterRelation, Proxy};
use itertools::Itertools;
use serde::Deserialize;

use crate::{errors::ChallengeListError, extractors::sessions::Authorization, models::accounts::OuterAccount, service::Response};
use crate::extractors::pagination::{Pagination, PaginationDefaults};
use crate::models::accounts::{Account, AccountGroup};
use crate::models::groups::{Group, Permissions};
use crate::schema::{accounts, group_assignments, groups};
use crate::service::database;
use crate::state::ChallengeListState;

#[get("/")]
pub async fn index(
    state: web::Data<ChallengeListState>,
    mut pagination: Pagination<OuterAccount, 50>,
    auth: Authorization
) -> impl Responder {
    pagination.with_defaults(PaginationDefaults::with(Some("id"), None));
    pagination.paginate(
        state,
        vec!["id"],
        vec!["position"],
        move |connection, params| {
            auth.0
                .account
                .has_or_throw(Permissions::MANAGE_ACCOUNTS)?;

            let base_query = accounts::table.select(Account::as_select());

            for (key, value) in params.filters.iter() {
                match key.as_str() {
                    _ => ()
                };
            }

            if params.sort.is_some() {
                let sort = params.sort.clone().unwrap();

                match sort.as_str() {
                    _ => ()
                };
            }

            Ok(base_query
                .limit(params.limit.into())
                .offset(params.page_offset())
                .load::<Account>(connection)?
                .iter()
                .map(|a| a.serialize(connection))
                .collect_vec())
        }
    ).await
}

#[get("/self")]
pub async fn get_self(
    auth: Authorization
) -> Result<Response<OuterAccount>, ChallengeListError> {
    Ok(Response {
        status: 200,
        message: None,
        data: Some(auth.0.account)
    })
}

#[get("/{id}")]
pub async fn get(
    state: web::Data<ChallengeListState>,
    path: web::Path<(String)>,
    auth: Authorization
) -> impl Responder {
    // TODO: this is temporary, until i believe it's fine to make accounts public
    auth.0
        .account
        .has_or_throw(Permissions::MANAGE_ACCOUNTS)?;

    database::execute(state, move |connection| {
        let (id) = path.into_inner();
        let account = accounts::table
            .find(id)
            .first::<Account>(connection)
            .optional()?;

        if account.is_none() {
            Err(ChallengeListError::ModelNotFound {
                model: "account"
            })
        } else {
            Ok(Response {
                status: 200,
                message: None,
                data: Some(account.unwrap().serialize(connection))
            })
        }
    }).await
}

#[derive(Deserialize)]
struct ModifiableAccount {
    pub username: Option<String>
}

fn patch_account(connection: &mut PooledConnection<ConnectionManager<PgConnection>>, account: Account, data: ModifiableAccount, is_self: bool) -> Result<Response<OuterAccount>, ChallengeListError> {
    let mut proxy = Proxy::from(account.clone());

    //if data.
    if proxy.has_changes() {
        diesel::update(accounts::table)
            .filter(accounts::id.eq(&account.id))
            .set(proxy.finalize())
            .execute(connection)?;

        Ok(Response {
            status: 200,
            message: None,
            data: Some(accounts::table
                .find(&account.id)
                .first::<Account>(connection)?
                .serialize(connection)
            )
        })
    } else {
        Ok(Response {
            status: 304,
            message: None,
            data: None
        })
    }
}

#[patch("/self")]
pub async fn update_self(
    state: web::Data<ChallengeListState>,
    data: web::Json<ModifiableAccount>,
    auth: Authorization,
) -> impl Responder {
    database::execute(state, move |connection| {
        patch_account(connection, auth.0.account.base.clone(), data.0, true)
    }).await
}

#[patch("/{id}")]
pub async fn update(
    state: web::Data<ChallengeListState>,
    path: web::Path<(String)>,
    data: web::Json<ModifiableAccount>,
    auth: Authorization,
) -> impl Responder {
    auth.0
        .account
        .has_or_throw(Permissions::MANAGE_ACCOUNTS)?;

    database::execute(state, move |connection| {
        let (id) = path.into_inner();
        let account = accounts::table
            .find(id)
            .first::<Account>(connection)
            .optional()?;

        if account.is_none() {
            Err(ChallengeListError::ModelNotFound {
                model: "account"
            })
        } else {
            patch_account(connection, account.unwrap(), data.0, false)
        }
    }).await
}

#[put("/{id}/groups/{group_id}")]
pub async fn add_group(
    state: web::Data<ChallengeListState>,
    path: web::Path<(String, String)>,
    auth: Authorization
) -> impl Responder {
    auth.0
        .account
        .has_or_throw(Permissions::MANAGE_ACCOUNT_GROUPS)?;

    database::execute(state, move |connection| {
        let (account_id, group_id) = path.into_inner();
        let account = accounts::table
            .find(&account_id)
            .first::<Account>(connection)
            .optional()?;

        if account.is_none() {
            return Err(ChallengeListError::ModelNotFound {
                model: "account"
            })
        }

        let account_groups = group_assignments::table
            .filter(group_assignments::account_id.eq(&account_id))
            .load::<AccountGroup>(connection)?;

        if account_groups.iter().any(|g| g.group_id == group_id.clone()) {
            return Err(ChallengeListError::GroupRegistered);
        }

        let group = groups::table
            .find(group_id)
            .first::<Group>(connection)
            .optional()?;

        if group.is_none() {
            return Err(ChallengeListError::ModelNotFound {
                model: "group"
            })
        }

        let account = account.unwrap();
        let group = group.unwrap();

        diesel::insert_into(group_assignments::table)
            .values((
                group_assignments::account_id.eq(&account.id),
                group_assignments::group_id.eq(&group.id)
            ))
            .execute(connection)?;

        Ok::<Response<String>, ChallengeListError>(Response {
            status: 201,
            message: None,
            data: None
        })
    }).await
}
#[delete("/{id}/groups/{group_id}")]
pub async fn remove_group(
    state: web::Data<ChallengeListState>,
    path: web::Path<(String, String)>,
    auth: Authorization
) -> impl Responder {
    auth.0
        .account
        .has_or_throw(Permissions::MANAGE_ACCOUNT_GROUPS)?;

    database::execute(state, move |connection| {
        let (account_id, group_id) = path.into_inner();
        let account = accounts::table
            .find(&account_id)
            .first::<Account>(connection)
            .optional()?;

        if account.is_none() {
            return Err(ChallengeListError::ModelNotFound {
                model: "account"
            })
        }

        let account_groups = group_assignments::table
            .filter(group_assignments::account_id.eq(&account_id))
            .load::<AccountGroup>(connection)?;

        if !account_groups.iter().any(|g| g.group_id == group_id.clone()) {
            return Err(ChallengeListError::GroupNotRegistered);
        }

        let group = groups::table
            .find(group_id)
            .first::<Group>(connection)
            .optional()?;

        if group.is_none() {
            return Err(ChallengeListError::ModelNotFound {
                model: "group"
            })
        }

        let account = account.unwrap();
        let group = group.unwrap();

        diesel::delete(group_assignments::table)
            .filter(group_assignments::account_id.eq(&account.id))
            .filter(group_assignments::group_id.eq(&group.id))
            .execute(connection)?;

        Ok(Response {
            status: 200,
            message: None,
            data: Some(group)
        })
    }).await
}
use actix_web::{get, Responder, web};
use diesel::{OptionalExtension, QueryDsl, RunQueryDsl, SelectableHelper, ExpressionMethods, PgConnection};
use diesel::r2d2::{ConnectionManager, PooledConnection};
use itertools::Itertools;
use crate::errors::ChallengeListError;
use crate::extractors::pagination::{Pagination, PaginationDefaults};
use crate::extractors::sessions::SafeAuthorization;
use crate::models::players::{Player};
use crate::models::records::{OuterRecord, Record, RecordStatus, RecordType};
use crate::schema::{players, records};
use crate::service::{database, Response};
use crate::state::ChallengeListState;

#[get("/")]
pub async fn index(
    state: web::Data<ChallengeListState>,
    mut pagination: Pagination<Player, 50>
) -> impl Responder {
    pagination.with_defaults(PaginationDefaults::with(Some("id"), None));
    pagination.paginate(
        state,
        vec!["id"],
        vec!["id", "name"],
        |connection, params| {
            let base_query = players::table.select(Player::as_select());

            for (key, value) in params.filters.iter() {

            }

            if params.sort.is_some() {
                let sort = params.sort.clone().unwrap();
            }

            Ok(base_query
                .limit(params.limit.into())
                .offset(params.page_offset())
                .load::<Player>(connection)?)
        }
    ).await
}

#[get("/{id}")]
pub async fn get(
    state: web::Data<ChallengeListState>,
    path: web::Path<(String)>
) -> impl Responder {
    database::execute(state, move |connection| {
        let (id) = path.into_inner();
        let player = players::table
            .find(id)
            .first::<Player>(connection)
            .optional()?;

        if player.is_none() {
            Err(ChallengeListError::ModelNotFound {
                model: "player"
            })
        } else {
            Ok(Response {
                status: 200,
                message: None,
                data: Some(player.unwrap())
            })
        }
    }).await
}

#[get("/{id}/profile")]
pub async fn get_profile(
    state: web::Data<ChallengeListState>,
    path: web::Path<(String)>
) -> impl Responder {
    database::execute(state, move |connection| {
        let (id) = path.into_inner();
        let player = players::table
            .find(&id)
            .first::<Player>(connection)
            .optional()?;

        if player.is_none() {
            Err(ChallengeListError::ModelNotFound {
                model: "player"
            })
        } else {
            let player = player.unwrap();
            Ok(Response {
                status: 200,
                message: None,
                data: Some(player.to_profile(connection))
            })
        }
    }).await
}


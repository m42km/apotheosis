use actix_web::{get, patch, post, web, Responder, delete, put};
use diesel::{expression::AsExpression, result::Error, Connection, Expression, ExpressionMethods, OptionalExtension, QueryDsl, RunQueryDsl, SelectableHelper, TextExpressionMethods, PgConnection, sql_query};
use diesel::sql_types::Integer;
use essence::{OuterRelation, Proxy};
use itertools::Itertools;
use serde::Deserialize;
use uuid::Uuid;

use crate::{
    errors::ChallengeListError,
    extractors::{
        pagination::{Pagination, PaginationDefaults},
        sessions::Authorization,
    },
    models::{
        challenges::{Challenge, InsertableChallenge, OuterChallenge},
        groups::Permissions,
        players::Player,
        records::{Record, RecordData, RecordStatus, RecordType},
    },
    schema::{challenge_creators, challenges, players},
    service::{database, Response},
    state::ChallengeListState,
};
use crate::models::challenges::ChallengeCreator;
use crate::schema::records;

fn get_player_or_throw(connection: &mut PgConnection, id: String, thrown: ChallengeListError) -> Result<Player, ChallengeListError> {
    let player = players::table
        .filter(players::id.eq(id))
        .first::<Player>(connection)
        .optional()?;

    if player.is_none() {
        Err(thrown)
    } else {
        Ok(player.unwrap())
    }
}

#[get("/")]
pub async fn index(
    state: web::Data<ChallengeListState>,
    mut pagination: Pagination<OuterChallenge, 50>,
) -> impl Responder {
    pagination.with_defaults(PaginationDefaults::with(Some("position"), None));
    pagination.paginate(
            state,
                  vec!["id", "position"],
                  vec!["name"],
                  |connection, params| {
            let base_query = challenges::table.select(Challenge::as_select());

            for (key, value) in params.filters.iter() {
                match key.as_str() {
                    "name" => base_query.filter(challenges::name.like(format!("%{}%", value))),
                    _ => todo!(), // should be impossible
                };
            }

            if params.sort.is_some() {
                let sort = params.sort.clone().unwrap();

                match sort.as_str() {
                    "position" => base_query.order(challenges::position.asc()),
                    _ => todo!() // should also
                };
            }
            
            Ok(base_query
                .limit(params.limit.into())
                .offset(params.page_offset())
                .load::<Challenge>(connection)?
                .iter()
                .map(|c| c.serialize(connection))
                .collect_vec())
        }).await
}

#[get("/{id}")]
pub async fn get(
    state: web::Data<ChallengeListState>,
    path: web::Path<(String)>
) -> impl Responder {
    database::execute(state, move |connection| {
        let (id) = path.into_inner();
        let challenge = challenges::table
            .find(id)
            .first::<Challenge>(connection)
            .optional()?;

        if challenge.is_none() {
            Err(ChallengeListError::ModelNotFound {
                model: "challenge"
            })
        } else {
            Ok(Response {
                status: 200,
                message: None,
                data: Some(challenge.unwrap().serialize(connection).with_records(connection)?)
            })
        }
    }).await
}

#[derive(Debug, Deserialize)]
pub struct ListParameters {
    #[serde(alias = "type")]
    pub _type: Option<String>,
}

#[get("/list")]
pub async fn list(
    state: web::Data<ChallengeListState>,
    query: web::Query<ListParameters>,
) -> impl Responder {
    database::execute(state, move |connection| {
        let challenges = match &query._type {
            Some(typ) => match typ.as_str() {
                "main" => {
                    Ok(sql_query(include_str!("../../sql/query_visible_list_challenges.sql"))
                        .bind::<Integer, _>(0)
                        .bind::<Integer, _>(100)
                        .load::<Challenge>(connection)?)
                }

                "legacy" => {
                    Ok(sql_query(include_str!("../../sql/query_visible_list_challenges.sql"))
                        .bind::<Integer, _>(100)
                        .bind::<Integer, _>(9999) // hopefully this won't be a future problem LOL
                        .load::<Challenge>(connection)?)
                },

                _ => Err(ChallengeListError::ListTypeParameterInvalid),
            },
            None => {
                Ok(challenges::table
                    .select(Challenge::as_select())
                    .order(challenges::position.asc())
                    .limit(100) // TODO: make this configurable
                    .load(connection)?)
            }
        }?;

        connection.transaction(|conn| {
            Ok::<Response<Vec<OuterChallenge>>, ChallengeListError>(Response {
                status: 200,
                message: None,
                data: Some(challenges
                    .iter()
                    .map(|c| c.serialize(conn))
                    .collect::<Vec<_>>()),
            })
        })
    }).await
}

#[get("/list/{position}")]
pub async fn list_position(
    state: web::Data<ChallengeListState>,
    path: web::Path<(i16)>
) -> impl Responder {
    database::execute(state, move |connection| {
        let (position) = path.into_inner();
        let challenge = challenges::table
            .filter(challenges::position.eq(position))
            .first::<Challenge>(connection)
            .optional()?;

        if challenge.is_none() {
            Err(ChallengeListError::ModelNotFound {
                model: "challenge"
            })
        } else {
            Ok(Response {
                status: 200,
                message: None,
                data: Some(challenge.unwrap().serialize(connection).with_records(connection)?)
            })
        }
    }).await
}

#[derive(Deserialize)]
struct ChallengeData {
    pub name: String,
    pub position: Option<usize>,
    pub verifier: String,
    pub creators: Vec<String>,
    pub publisher: String,
    pub video: String,
    pub fps: Option<String>,
}

#[post("/")]
pub async fn create(
    state: web::Data<ChallengeListState>,
    auth: Authorization,
    data: web::Json<ChallengeData>,
) -> impl Responder {
    &auth
        .0
        .account
        .has_or_throw(Permissions::MANAGE_CHALLENGES)?;

    database::execute(state, move |connection| {
        let challenges = challenges::table
            .select(Challenge::as_select())
            .order(challenges::position.asc())
            .load::<Challenge>(connection)?;
        let position = data.position.unwrap_or(challenges.len() + 1);

        if position < 1 || position > (challenges.len() + 1) {
            return Err(ChallengeListError::PositionTooGreatOrSmall {
                lowest: 1,
                highest: challenges.len() + 1,
            });
        }

        let mut players = data.creators.clone();
        players.append(&mut vec![data.verifier.clone(), data.publisher.clone()]);
        let players: Vec<_> = players.into_iter().unique().collect();

        let mut existing_players = players::table
            .select(Player::as_select())
            .filter(players::username.eq_any(players.clone()))
            .load::<Player>(connection)?;

        let needed_players = players
            .iter()
            .filter(|x| !existing_players.iter().any(|p| p.username == **x))
            .map(|x| x.clone())
            .collect();

        println!("{:?}", needed_players);

        existing_players.append(&mut Player::create_many(connection, needed_players)?);
        connection.transaction(|conn| {
            let mut enumerator = challenges.iter().enumerate();
            let id = format!("{}", Uuid::new_v4());
            enumerator.advance_by(position - 1);

            for (pos, challenge) in enumerator {
                let position: i16 = (pos + 2).try_into().unwrap();
                println!("{}", position);

                diesel::update(challenges::table)
                    .filter(challenges::id.eq(&challenge.id))
                    .set(challenges::position.eq(position))
                    .execute(conn)?;
            }

            let verifier = existing_players
                .iter()
                .find(|p| p.username == data.verifier)
                .unwrap();

            let publisher = existing_players
                .iter()
                .find(|p| p.username == data.publisher)
                .unwrap();

            let creators: Vec<_> = existing_players
                .iter()
                .filter(|x| data.creators.iter().any(|p| *p == x.username))
                .collect();

            let challenge = Challenge::create(
                conn,
                InsertableChallenge {
                    id: &id,
                    name: &data.name,
                    position: &position.try_into().unwrap(),
                    video: &data.video,
                    verifier_id: &verifier.id,
                    publisher_id: &publisher.id,
                    level_id: None,
                },
            )?;

            for creator in creators.iter() {
                diesel::insert_into(challenge_creators::table)
                    .values((
                        challenge_creators::challenge_id.eq(&challenge.id),
                        challenge_creators::player_id.eq(&creator.id),
                    ))
                    .execute(conn)?;
            }

            Record::create(
                conn,
                auth.0.account.base,
                challenge.clone(),
                RecordData {
                    player: verifier.clone(),
                    typ: RecordType::Verification,
                    status: Some(RecordStatus::Approved),
                    video: data.video.clone(),
                    raw_footage: None, // TODO: allow raw footage in challenge post
                },
            )?;

            Ok(Response {
                status: 201,
                message: None,
                data: Some(challenge.serialize(conn).with_records(conn)?),
            })
        })
    }).await
}

#[derive(Deserialize)]
struct ModifiableChallenge {
    pub name: Option<String>,
    pub position: Option<i16>,
    pub verifier: Option<String>,
    pub publisher: Option<String>,
    pub video: Option<String>
}

#[patch("/{id}")]
pub async fn update(
    state: web::Data<ChallengeListState>,
    auth: Authorization,
    path: web::Path<(String)>,
    data: web::Json<ModifiableChallenge>,
) -> impl Responder {
    &auth
        .0
        .account
        .has_or_throw(Permissions::MANAGE_CHALLENGES)?;

    database::execute(state, move |connection| {
        let (id) = path.into_inner();
        let challenge = challenges::table
            .filter(challenges::id.eq(&id))
            .first::<Challenge>(connection)
            .optional()?;

        if challenge.is_none() {
            return Err(ChallengeListError::ModelNotFound { model: "challenge" });
        }

        let challenge = challenge.unwrap();
        let mut proxy = Proxy::from(challenge.clone());

        if data.name.is_some() {
            let name = data.name.as_ref().unwrap();

            proxy.set("name", name.clone());
        }

        if data.position.is_some() {
            let position = data.position.as_ref().unwrap().clone();

            if position != challenge.position {
                // only modify the challenge if it's truly not the same
                let mut challenges = challenges::table
                    .select(Challenge::as_select())
                    .order(challenges::position.asc())
                    .load::<Challenge>(connection)?;

                if position < 1 || position > (challenges.len() + 1).try_into().unwrap() {
                    return Err(ChallengeListError::PositionTooGreatOrSmall {
                        lowest: 1,
                        highest: challenges.len() + 1,
                    });
                }

                connection.transaction(|conn| {
                    let position_as_idx: usize = (position - 1).try_into().unwrap();
                    let current = challenges.remove(
                        challenges
                            .iter()
                            .position(|c| c.id == challenge.id)
                            .unwrap(),
                    );

                    challenges.insert(position_as_idx, current.clone());

                    for (pos, actual) in challenges.iter().enumerate() {
                        if &actual.id == &current.id {
                            continue;
                        }

                        let position  : i16 = (pos + 1).try_into().unwrap();

                        diesel::update(challenges::table)
                            .filter(challenges::id.eq(&actual.id))
                            .set(challenges::position.eq(position))
                            .execute(conn)?;
                    }

                    Ok::<(), ChallengeListError>(())
                })?;

                proxy.set("position", position.clone());
            }
        }
        
        if data.publisher.is_some() {
            let publisher = get_player_or_throw(connection, data.publisher.as_ref().unwrap().clone(), ChallengeListError::ReferencedModelNotFound {
                model: "player",
                parameter: "publisher"
            })?;

            proxy.set("publisher_id", publisher.id.clone());
        }

        if data.verifier.is_some() {
            let challenge = challenge.clone();
            let verifier = get_player_or_throw(connection, data.publisher.as_ref().unwrap().clone(), ChallengeListError::ReferencedModelNotFound {
                model: "player",
                parameter: "verifier"
            })?;

            // delete the original verifier's record
            diesel::delete(records::table)
                .filter(records::player_id.eq(&challenge.verifier_id))
                .filter(records::challenge_id.eq(&challenge.id))
                .execute(connection)?;

            // create a new record
            Record::create(
                connection,
                auth.0.account.base,
                challenge.clone(),
                RecordData {
                    player: verifier.clone(),
                    typ: RecordType::Verification,
                    raw_footage: None,
                    status: Some(RecordStatus::Approved),
                    video: if data.video.is_some() {
                        let video = data.video.clone().unwrap();

                        proxy.set("video", video.clone());
                        video
                    } else {
                        challenge.video
                    }
                }
            )?;

            proxy.set("verifier_id", verifier.id);
        }

        if data.video.is_some() && !proxy.is_changed("video") {

        }

        if proxy.has_changes() {
            diesel::update(challenges::table)
                .filter(challenges::id.eq(&id))
                .set(proxy.finalize())
                .execute(connection)?;

            Ok(Response {
                status: 200,
                message: None,
                data: Some(
                    challenges::table
                        .filter(challenges::id.eq(&id))
                        .first::<Challenge>(connection)?
                        .serialize(connection)
                )
            })
        } else {
            Ok(Response {
                status: 304,
                message: None,
                data: None
            })
        }
    }).await
}

#[put("/{id}/creators/{creator_id}")]
pub async fn add_creator(
    state: web::Data<ChallengeListState>,
    auth: Authorization,
    path: web::Path<(String, String)>,
) -> impl Responder {
    &auth
        .0
        .account
        .has_or_throw(Permissions::MANAGE_CHALLENGES)?;

    database::execute(state, move |connection| {
        let (id, player) = path.into_inner();
        let challenge = challenges::table
            .filter(challenges::id.eq(&id))
            .first::<Challenge>(connection)
            .optional()?;

        if challenge.is_none() {
            return Err(ChallengeListError::ModelNotFound { model: "challenge" });
        }

        let challenge = challenge.unwrap();
        let creators = challenge_creators::table
            .filter(challenge_creators::challenge_id.eq(&challenge.id))
            .load::<ChallengeCreator>(connection)?;

        let creator = get_player_or_throw(connection, player, ChallengeListError::ModelNotFound {
            model: "player"
        })?;

        if creators.iter().any(|cc| cc.player_id == creator.id) {
            return Err(ChallengeListError::CreatorRegistered);
        }

        diesel::insert_into(challenge_creators::table)
            .values((
                challenge_creators::challenge_id.eq(&challenge.id),
                challenge_creators::player_id.eq(&creator.id),
            ))
            .execute(connection)?;

        Ok::<Response<String>, ChallengeListError>(Response {
            status: 201,
            message: None,
            data: None
        })
    }).await
}
#[delete("/{id}/creators/{creator_id}")]
pub async fn remove_creator(
    state: web::Data<ChallengeListState>,
    auth: Authorization,
    path: web::Path<(String, String)>,
) -> impl Responder {
    &auth
        .0
        .account
        .has_or_throw(Permissions::MANAGE_CHALLENGES)?;

    database::execute(state, move |connection| {
        let (id, player) = path.into_inner();
        let challenge = challenges::table
            .filter(challenges::id.eq(&id))
            .first::<Challenge>(connection)
            .optional()?;

        if challenge.is_none() {
            return Err(ChallengeListError::ModelNotFound { model: "challenge" });
        }

        let challenge = challenge.unwrap();
        let creators = challenge_creators::table
            .filter(challenge_creators::challenge_id.eq(&challenge.id))
            .load::<ChallengeCreator>(connection)?;

        let creator = get_player_or_throw(connection, player, ChallengeListError::ModelNotFound {
            model: "player"
        })?;

        if !creators.iter().any(|cc| cc.player_id == creator.id) {
            return Err(ChallengeListError::CreatorNotRegistered);
        }

        diesel::delete(challenge_creators::table)
            .filter(challenge_creators::challenge_id.eq(&challenge.id))
            .filter(challenge_creators::player_id.eq(&creator.id))
            .execute(connection)?;

        Ok::<Response<String>, ChallengeListError>(Response {
            status: 204,
            message: None,
            data: None
        })
    }).await
}


#[delete("/{id}")]
pub async fn delete(
    state: web::Data<ChallengeListState>,
    auth: Authorization,
    path: web::Path<(String)>,
) -> impl Responder {
    auth.0
        .account
        .has_or_throw(Permissions::DELETE_CHALLENGES)?;

    // TODO: maybe add a confirmation?
    database::execute(state, move |connection| {
        let (id) = path.into_inner();
        let challenge = challenges::table
            .filter(challenges::id.eq(&id))
            .first::<Challenge>(connection)
            .optional()?;

        if challenge.is_none() {
            return Err(ChallengeListError::ModelNotFound { model: "challenge" });
        }

        Ok(Response{
            status: 200,
            message: None,
            data: Some(challenge.unwrap())
        })
    }).await
}
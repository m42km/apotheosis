use actix_web::{web, get, Responder, patch, delete};
use crate::extractors::pagination::{Pagination, PaginationDefaults};
use crate::models::records::{OuterRecord, OuterRecordWithSubmitter, Record, RecordStatus, RecordType};
use crate::schema::records;
use crate::state::ChallengeListState;
use diesel::{QueryDsl, RunQueryDsl, SelectableHelper, ExpressionMethods, OptionalExtension};
use essence::{OuterRelation, Proxy};
use itertools::Itertools;
use serde::Deserialize;
use crate::errors::ChallengeListError;
use crate::extractors::sessions::{Authorization, SafeAuthorization};
use crate::models::groups::Permissions;
use crate::schema::accounts::dsl::accounts;
use crate::service::{database, Response};

#[get("/")]
pub async fn index(
    state: web::Data<ChallengeListState>,
    auth: SafeAuthorization,
    mut pagination: Pagination<OuterRecordWithSubmitter, 50>
) -> impl Responder {
    pagination.with_defaults(PaginationDefaults::with(Some("id"), None));
    pagination.paginate(
        state,
        vec!["id"],
        vec!["id", "status", "submitter"],
        move |connection, params| {
            let base_query = records::table.select(Record::as_select());
            let mut status_present = false;
            let auth_1 = auth.0.clone();

            for (key, value) in params.filters.iter() {
                match key.as_str() {
                    "status" => {
                        let status: RecordStatus = value.clone().into();

                        if status == RecordStatus::Unknown {
                            return Err(ChallengeListError::BadRequest);
                        }

                        if status == RecordStatus::Submitted || status == RecordStatus::Rejected || status == RecordStatus::UnderConsideration {
                            status_present = true;
                            if auth_1.is_none() {
                                return Err(ChallengeListError::Unauthorized);
                            }

                            // hate it
                            auth_1.clone().unwrap().account.has_or_throw(Permissions::MANAGE_RECORDS)?;

                            base_query.filter(records::status.eq(i16::from(status)));
                        }
                    },
                    "submitter" => {
                        if auth.0.is_none() {
                            return Err(ChallengeListError::Unauthorized);
                        }

                        if !auth.0.clone().unwrap().account.has_any(vec![Permissions::MANAGE_RECORDS]) {
                            return Err(ChallengeListError::Forbidden);
                        }

                        base_query.filter(records::submitter_id.eq(value));
                    }
                    _ => ()
                }
            }

            if !status_present  {
                base_query.filter(records::status.eq(i16::from(RecordStatus::Approved)));
            }

            Ok(base_query
                .limit(params.limit.into())
                .offset(params.page_offset())
                .load(connection)?
                .iter()
                .map(|r| {
                    if auth.0.is_some() {
                        let auth = auth.0.clone().unwrap();

                        if auth.account.has_any(vec![Permissions::MANAGE_RECORDS]) {
                            r.serialize(connection).with_submitter_unignored()
                        } else {
                            r.serialize(connection).with_submitter_ignored()
                        }
                    } else {
                        r.serialize(connection).with_submitter_ignored()
                    }
                })
                .collect_vec())
        }
    ).await
}

#[get("/{id}")]
pub async fn get(
    state: web::Data<ChallengeListState>,
    path: web::Path<(String)>,
    auth: SafeAuthorization
) -> impl Responder {
    database::execute(state, move |connection| {
        let (id) = path.into_inner();
        let record = records::table
            .find(id)
            .first::<Record>(connection)
            .optional()?;

        if record.is_none() {
            return Err(ChallengeListError::ModelNotFound {
                model: "record"
            });
        }

        let record = record.unwrap();

        if record.status != i16::from(RecordStatus::Approved) {
            Ok(auth.execute_or(|account| {
                account.has_or_throw(Permissions::MANAGE_RECORDS);

                Ok(Response {
                    status: 200,
                    message: None,
                    data: Some(record.serialize(connection).with_submitter_unignored())
                })
            })?)
        } else {
            Ok(Response {
                status: 200,
                message: None,
                data: Some(record.serialize(connection).with_submitter_ignored())
            })
        }
    }).await
}

#[derive(Deserialize)]
struct ModifiableRecord {
    pub video: Option<String>,
    pub status: Option<i16>
}

#[patch("/{id}")]
pub async fn update(
    state: web::Data<ChallengeListState>,
    path: web::Path<(String)>,
    auth: SafeAuthorization,
    data: web::Json<ModifiableRecord>
) -> impl Responder {
    if auth.0.is_none() {
        return Err(ChallengeListError::Unauthorized)
    }

    database::execute(state, move |connection| {
        let (id) = path.into_inner();
        let record = records::table
            .find(id)
            .first::<Record>(connection)
            .optional()?;

        if record.is_none() {
            return Err(ChallengeListError::ModelNotFound {
                model: "record"
            });
        }

        let record = record.unwrap();
        let submitter = auth.0.clone().unwrap();
        let mut proxy = Proxy::from(record.clone());
        let is_author = record.submitter_id == submitter.account.base.id;

        if !is_author && !submitter.account.has(Permissions::MANAGE_RECORDS) {
            return Err(ChallengeListError::Forbidden);
        }

        if data.status.is_some() {
            let status = RecordStatus::from(data.status.unwrap().to_string());

            auth.execute_or(|account| {
                account.has_or_throw(Permissions::MANAGE_RECORDS)?;

                proxy.set("status", i16::from(status));

                Ok(())
            })?;
        }

        if proxy.has_changes() {
            diesel::update(records::table)
                .filter(records::id.eq(&record.id))
                .set(proxy.finalize())
                .execute(connection)?;

            Ok(Response {
                status: 200,
                message: None,
                data: Some(records::table
                    .filter(records::id.eq(&record.id))
                    .first::<Record>(connection)?
                    .serialize(connection))
            })
        } else {
            Ok(Response {
                status: 304,
                message: None,
                data: None
            })
        }
    }).await
}

#[delete("/{id}")]
pub async fn delete(
    state: web::Data<ChallengeListState>,
    path: web::Path<(String)>,
    auth: Authorization
) -> impl Responder {
    auth.0
        .account
        .has_or_throw(Permissions::MANAGE_RECORDS)?;

    database::execute(state, move |connection| {
        let (id) = path.into_inner();
        let record = records::table
            .find(id)
            .first::<Record>(connection)
            .optional()?;

        if record.is_none() {
            return Err(ChallengeListError::ModelNotFound {
                model: "record"
            });
        }

        let record = record.unwrap();
        
        if record.type_ == i16::from(RecordType::Verification) {
            return Err(ChallengeListError::RecordIsVerification);
        }

        diesel::delete(records::table)
            .filter(records::id.eq(&record.id))
            .execute(connection);

        Ok(Response{
            status: 200,
            message: None,
            data: Some(record)
        })
    }).await
}
#![allow(trivial_bounds)]
#![feature(trivial_bounds)]
#![feature(iter_advance_by)]
#![feature(core_intrinsics)]
#![recursion_limit = "32"] // not making htis mistake again

use std::{env, time::SystemTime};
use std::fs::create_dir_all;
use actix_cors::Cors;

use actix_web::{
    dev, guard, middleware::ErrorHandlerResponse, web, App, HttpMessage, HttpResponse, HttpServer,
};
use actix_web::http::header;
use dotenvy::dotenv;
use errors::ChallengeListError;
use state::ChallengeListState;

pub mod errors;
pub mod extractors;
pub mod models;
pub mod routes;
pub mod schema;
pub mod service;
pub mod state;
pub mod util;

fn init_dir() {
    // data directory
    create_dir_all("data/avatars");
    create_dir_all("data/banners");
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    dotenv().expect("A proper environmental file has not been found");
    init_dir();

    HttpServer::new(|| {
        App::new()
            .app_data(
                web::JsonConfig::default()
                    .error_handler(|err, _| ChallengeListError::from(err).into()),
            )
            .app_data(web::Data::new(ChallengeListState::new()))
            .wrap(Cors::default()
                .allow_any_origin()
                .allowed_methods(vec!["GET","POST","PUT","PATCH","DELETE"])
                .allowed_headers(vec![
                    header::CONTENT_TYPE,
                    header::CONTENT_LENGTH,
                    header::AUTHORIZATION,
                    header::COOKIE
                ])
                .supports_credentials())
            // /accounts
            .service(
                web::scope("/accounts")
                    .service(routes::accounts::index)
                    .service(routes::accounts::get_self)
                    .service(routes::accounts::get)
                    .service(routes::accounts::update_self)
                    .service(routes::accounts::update)
                    .service(routes::accounts::add_group)
                    .service(routes::accounts::remove_group)
                    //.service(routes::accounts::delete_self)
                    //.service(routes::accounts::delete)
            )
            // /groups
            .service(
                web::scope("/groups")
            )
            // challenges
            .service(
                web::scope("/challenges")
                    .service(routes::challenges::index)
                    .service(routes::challenges::list)
                    .service(routes::challenges::list_position)
                    .service(routes::challenges::create)
                    .service(routes::challenges::get)
                    .service(routes::challenges::update)
                    .service(routes::challenges::add_creator)
                    .service(routes::challenges::remove_creator)
                    //.service(routes::challenges::delete)
            )
            // records
            .service(
                web::scope("/records")
                    .service(routes::records::index)
                    //.service(routes::records::create)
                    .service(routes::records::get)
                    .service(routes::records::update)
                    .service(routes::records::delete)
                    // /records/:id/notes
                    .service(routes::record_notes::index)
                    .service(routes::record_notes::create)
                    .service(routes::record_notes::update)
                    .service(routes::record_notes::delete)
            )
            // /players
            .service(
                web::scope("/players")
                    .service(routes::players::index)
                    .service(routes::players::get)
                    .service(routes::players::get_profile)
                    //.service(routes::players::update)
                    //.service(routes::players::claim)
            )
            // /packs
            .service(
                web::scope("/packs")
                    //.service(routes::packs::index
                    //.service(routes::packs::create)
                    //.service(routes::packs::get)
                    //.service(Routes::packs::delete)
            )
            // /authorization
            .service(
                web::scope("/authorization")
                    .service(routes::authorization::register)
                    .service(routes::authorization::login),
            )
            // /meta
            .service(
                web::scope("/meta")
                    .service(routes::meta::team)
            )
            .default_service(web::route().to(routes::handle_default))
    })
    .workers(4) // prevent there from being far too many db pool connections
    .bind((
        "127.0.0.1",
        env::var("PORT")
            .expect("A port must be provided to run this application")
            .parse::<u16>()
            .unwrap(),
    ))?
    .run()
    .await
}

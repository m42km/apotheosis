use dotenvy::{var, Error};
use std::{fmt::Debug, str::FromStr};

/// A structure containing all of the possible
/// configured values
#[derive(Default, Clone)]
pub struct ChallengeListConfig {
    pub database_url: String,
}

impl ChallengeListConfig {
    pub fn new() -> ChallengeListConfig {
        let mut conf = ChallengeListConfig::default();
        conf.initialize();

        conf
    }

    pub fn initialize(&mut self) -> () {
        self.database_url = self.get("DATABASE_URL").unwrap();
    }

    pub fn get<T: FromStr>(&self, key: &str) -> Result<T, Error>
    where
        <T as FromStr>::Err: Debug,
    {
        match var(key) {
            Ok(value) => Ok(value.parse().unwrap()),
            Err(err) => Err(err),
        }
    }

    pub fn get_default<T: FromStr>(&self, key: &str, or_default: T) -> T
    where
        <T as FromStr>::Err: Debug,
    {
        match var(key) {
            Ok(value) => value.parse().unwrap(),
            Err(_) => or_default,
        }
    }
}

use argon2::{PasswordHasher, password_hash::{rand_core::OsRng, SaltString}, Argon2};
use diesel::{
    r2d2::{ConnectionManager, Pool},
    PgConnection,
};

use self::config::ChallengeListConfig;

mod config;

/// A global state containing everything about the
/// Challenge List API as an application.
///
/// Handles initial database pool setup, as well as
/// environment file reading.
pub struct ChallengeListState {
    /// The configuration of the application.
    pub config: ChallengeListConfig,
    /// A pool of connections to the PostgreSQL
    /// database.
    pub pool: Pool<ConnectionManager<PgConnection>>,
}

impl ChallengeListState {
    /// Creates a new instance of the application
    /// state.
    pub fn new() -> ChallengeListState {
        // get the current application configuration
        // we're only putting it here as we need to access it later
        let config = ChallengeListConfig::new();

        ChallengeListState {
            config: config.clone(),
            pool: Pool::builder()
                .build(ConnectionManager::<PgConnection>::new(&config.database_url))
                .expect("A valid database URL should be provided to the application"),
        }
    }

    pub fn hash_password(&self, password_bytes: &[u8]) -> String {
        Argon2::default().hash_password(password_bytes, &SaltString::generate(&mut OsRng)).unwrap().to_string()
    }
}

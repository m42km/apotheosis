-- Your SQL goes here

CREATE TABLE "challenge_creators"(
	"challenge_id" VARCHAR NOT NULL,
	"player_id" VARCHAR NOT NULL,
	
	PRIMARY KEY("challenge_id", "player_id")
);

ALTER TABLE "challenge_creators"
    ADD CONSTRAINT fk_creators_challenge
        FOREIGN KEY(challenge_id)
            REFERENCES challenges(id)
            ON DELETE CASCADE;

ALTER TABLE "challenge_creators"
    ADD CONSTRAINT fk_creators_player
        FOREIGN KEY(player_id)
            REFERENCES players(id)
            ON DELETE NO ACTION;

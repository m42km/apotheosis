-- Your SQL goes here
ALTER TABLE "groups"
    ALTER COLUMN "flags" DROP NOT NULL;

ALTER TABLE IF EXISTS "challenges"
    ALTER COLUMN "flags" DROP NOT NULL;

ALTER TABLE "accounts"
    ALTER COLUMN "flags" DROP NOT NULL;
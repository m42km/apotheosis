-- Your SQL goes here

CREATE TABLE "record_notes" (
	"id" VARCHAR NOT NULL PRIMARY KEY,
	"record_id" VARCHAR NOT NULL,
	"submitter_id" VARCHAR NOT NULL,
	"submitted_at" INT8 NOT NULL,
	"updated_at" INT8 NOT NULL,
    "content" VARCHAR NOT NULL,
	"public" BOOL NOT NULL
);

ALTER TABLE "record_notes"
    ADD CONSTRAINT fk_record_notes_submitter
        FOREIGN KEY(submitter_id)
            REFERENCES accounts(id)
            ON DELETE CASCADE;

-- This file should undo anything in `up.sql`
DROP TABLE IF EXISTS "records";
DROP TABLE IF EXISTS "challenges";
DROP TABLE IF EXISTS "players";
DROP TABLE IF EXISTS "group_assignments";
DROP TABLE IF EXISTS "groups";
DROP TABLE IF EXISTS "accounts";

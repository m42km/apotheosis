CREATE TABLE "accounts" (
	"id" VARCHAR NOT NULL PRIMARY KEY,
	"username" VARCHAR(32) NOT NULL,
	"password_hash" VARCHAR(128) NOT NULL,
	"created_at" INT8 NOT NULL,
	"updated_at" INT8 NOT NULL,
	"country_code" VARCHAR(2),
	"subdivision_code" VARCHAR(8),
	"flags" INT4 NOT NULL
);

CREATE UNIQUE INDEX pk_accounts_unique
ON "accounts" ("username");

CREATE TABLE "groups" (
	"id" VARCHAR NOT NULL PRIMARY KEY,
	"name" VARCHAR(32) NOT NULL,
	"priority" INT2 NOT NULL,
	"color" VARCHAR(6) NOT NULL,
	"permissions" INT4 NOT NULL
);

CREATE TABLE "group_assignments" (
	"account_id" VARCHAR NOT NULL REFERENCES accounts(id),
	"group_id" VARCHAR NOT NULL REFERENCES groups(id),

	PRIMARY KEY(account_id, group_id)
);

CREATE TABLE "players" (
	"id" VARCHAR NOT NULL PRIMARY KEY,
	"name" VARCHAR(24) NOT NULL,
	"banned" BOOL NOT NULL,
	"account_id" VARCHAR REFERENCES accounts(id),
	"flags" INT4 NOT NULL
);

ALTER TABLE "players"
ADD CONSTRAINT fk_player_account
	FOREIGN KEY(account_id)
		REFERENCES accounts(id)
		ON DELETE SET NULL;

CREATE TABLE "challenges" (
	"id" VARCHAR NOT NULL PRIMARY KEY,
	"name" VARCHAR(64) NOT NULL,
	"position" INT2 NOT NULL,
	"video" TEXT NOT NULL,
	"verifier_id" VARCHAR NOT NULL,
	"publisher_id" VARCHAR NOT NULL,
	"level_id" VARCHAR
);

ALTER TABLE "challenges"
ADD CONSTRAINT fk_challenge_verifier
	FOREIGN KEY(verifier_id)
		REFERENCES players(id)
		ON DELETE SET NULL;

ALTER TABLE "challenges"
ADD CONSTRAINT fk_challenge_publisher
	FOREIGN KEY(publisher_id)
		REFERENCES players(id)
		ON DELETE SET NULL;

CREATE TABLE "records" (
	"id" VARCHAR NOT NULL PRIMARY KEY,
	"challenge_id" VARCHAR NOT NULL,
	"player_id" VARCHAR NOT NULL,
	"submitter_id" VARCHAR NOT NULL,
	"submitted_at" INT8 NOT NULL,
	"updated_at" INT8 NOT NULL,
	"video" TEXT NOT NULL,
	"status" INT2 NOT NULL,
	"type" INT2 NOT NULL
);

ALTER TABLE "records"
ADD CONSTRAINT fk_record_challenge
	FOREIGN KEY(challenge_id)
		REFERENCES challenges(id)
		ON DELETE CASCADE;

ALTER TABLE "records"
ADD CONSTRAINT fk_record_player
	FOREIGN KEY(player_id)
		REFERENCES players(id)
		ON DELETE CASCADE;
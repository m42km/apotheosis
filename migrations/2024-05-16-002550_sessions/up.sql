-- Your SQL goes here

CREATE TABLE "sessions" (
	"id" VARCHAR NOT NULL PRIMARY KEY,
	"account_id" VARCHAR NOT NULL,
	"token" VARCHAR NOT NULL,
	"created_at" INT8 NOT NULL,
	"expires_at" INT8 NOT NULL
);


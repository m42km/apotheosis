<div align="center">
    <img src="https://cdn.discordapp.com/attachments/1132176035216113804/1243468930916421652/Group_2.png?ex=6651962f&is=665044af&hm=fe4cf912aad010f3f3703feeb0d9ca571a202cfca00a264909b79204b035901f&" />
    <p>A new generation, and approach to a pinnacle to the development of a list website.</p>
    <hr />
</div>

Welcome to the repository of **Apotheosis**, the backend to the newer version of the Challenge List website, succeeding it's previous counterpart that utilized the [Pointercrate](https://github.com/stadust/pointercrate) backend/frontend. This project aims to improve upon everything the original website lacked, and add new and improve features to have the site serve more as an overarching stepping ground for challenges at a whole, representing more than just the Challenge List.

Currently, it is in a strong work in progress state, and is far from production ready. Developement is internalized, and thus it's path is made unclear currently.

## Compilation Requirements
* Rust (Nightly) 1.7x.x or above,
* PostgreSQL 12 or Above,
* and more to be documented later.

## Contributions
All contributions and pull requests are welcome, however; the direction of this project is not made clear, and will require asking around to figure out unless you know your way around the project already.

With that in mind, feel free to try anyways, and make an issue describing any feature requests, bug reports, or other similar issues you may have with the API.

### Support

All support for the API is handled here on GitLab. If you have any questions, please open an issue as mentioned before, and we will get back to you as soon as possible.

## License

challengelist/apotheosis is licensed under the AGPL-3.0 license. Please see the license file for more information. Simply put, you are free to use this software for any purpose, but you must disclose any changes you make to the source code and release them under the same license. This however does not include branding or other assets that are not part of the source code; such as Challenge List logos, icons, etc.

[tl;dr](https://tldrlegal.com/license/gnu-affero-general-public-license-v3-(agpl-3.0))
use bevy_reflect::{GetField, Reflect, Struct};

use diesel::{r2d2::{ConnectionManager, PooledConnection, R2D2Connection}, PgConnection};
pub use essence_impl::*;
pub trait Proxyable {
    fn field_names() -> Vec<String>;
}

pub struct Proxy<T>
where
    T: Proxyable + Clone + Reflect,
{
    internal: T,
    changes: Vec<String>
}

impl<T> Proxy<T>
where
    T: Proxyable + Clone + Reflect + Struct,
{
    pub fn from(object: T) -> Proxy<T> {
        Proxy {
            internal: object.clone(), // create a clone of the object to not affect the original
            changes: vec![]
        }
    }
    
    pub fn set<U>(&mut self, key: &str, value: U) -> &Proxy<T>
        where U : Reflect + PartialEq,
    {
        let field = self.internal.get_field_mut::<U>(key).unwrap();

        if *field == value {
            return self;
        }

        // this doesn't deserve to work
        *field = value;
        self.changes.push(key.to_string());

        self
    }

    pub fn has_changes(&self) -> bool {
        self.changes.len() > 0
    }

    pub fn is_changed(&self, key: &str) -> bool {
        self.changes.contains(&key.to_string())
    }

    pub fn finalize(&self) -> T {
        self.internal.clone()
    }
}

pub trait Outer {
    type Output;

    fn serialize(&self) -> Self::Output;
    //fn serialize(&self, allow_safe: bool) -> Self::Output;
}

pub trait OuterRelation
{
    type Output;

    // TODO: Support other connections.
    fn serialize(&self, connection: &mut PooledConnection<ConnectionManager<PgConnection>>) -> Self::Output;
    //fn serialize(&self, connection: &mut PooledConnection<ConnectionManager<PgConnection>>, ignore_safe: bool) -> Self::Output;
}
use std::path;

use proc_macro::TokenStream;
use proc_macro2::{Literal, Punct, Span};
use proc_macro_crate::crate_name;
use quote::{quote, ToTokens, TokenStreamExt};
use syn::{
    self, braced,
    parse::{Parse, ParseBuffer, ParseStream},
    parse_macro_input,
    punctuated::Punctuated,
    token::Type,
    Attribute, DeriveInput, Expr, GenericArgument, Ident, LitStr, Meta, PathArguments, Token,
    TypePath,
};

#[proc_macro_derive(Proxyable)]
pub fn get_fields(input: TokenStream) -> TokenStream {
    let struct_ast: syn::DeriveInput = syn::parse(input).unwrap();
    let name = &struct_ast.ident;
    let fields = match struct_ast.data {
        syn::Data::Struct(ref data) => match data.fields {
            syn::Fields::Named(ref fields) => fields.named.iter().map(|f| f.ident.clone().unwrap()),
            _ => unimplemented!(),
        },
        _ => unimplemented!(),
    };
    let generated_ast = quote! {
        impl essence::Proxyable for #name {
            fn field_names() -> Vec<String> {
                let mut vec = Vec::new();

                #(
                    vec.push(stringify!(#fields).to_string());
                )*

                return vec;
            }
        }
    };

    generated_ast.into()
}

#[derive(Debug, Clone)]
struct ModelDecl {
    pub name: Ident,
    pub fields: Punctuated<FieldDef, Token![,]>,
    pub diesel_table: Option<Expr>,
    pub meta: Vec<Attribute>,
}

impl Parse for ModelDecl {
    #[inline]
    fn parse(input: ParseStream) -> Result<ModelDecl, syn::Error> {
        let meta = syn::Attribute::parse_outer(input)?;
        let model_name = input.parse::<Ident>()?;
        let model_content;
        let _brace = braced!(model_content in input);
        let fields = Punctuated::parse_terminated(&model_content)?;

        let is_diesel_present = crate_name("diesel");
        let diesel_table = if is_diesel_present.is_ok() {
            let mut attribute = None;

            for attr in meta.clone() {
                match attr.clone().meta {
                    Meta::NameValue(nv) => {
                        if attr.path().get_ident().unwrap() == "table" {
                            attribute = Some(nv);
                        }
                    }
                    _ => (),
                }
            }

            if attribute.is_some() {
                Some(attribute.unwrap().value.clone())
            } else {
                None
            }
        } else {
            None
        };

        Ok(ModelDecl {
            name: model_name,
            fields,
            diesel_table,
            meta,
        })
    }
}

impl ModelDecl {
    pub fn get_fields(&self) -> Vec<&FieldDef> {
        self.fields.iter().filter(|f| !f.is_relation).collect()
    }

    pub fn get_serialized_fields(&self) -> Vec<&FieldDef> {
        self.fields
            .iter()
            .filter(|f| !f.is_optionally_ignored && f.is_relation)
            .collect()
    }

    pub fn get_relation_fields(&self) -> Vec<RelationFieldDef> {
        self.fields
            .iter()
            .filter(|f| !f.is_optionally_ignored && f.is_relation)
            .map(|f| RelationFieldDef { field: f.clone() })
            .collect()
    }


    pub fn get_relation_grabbers(&self) -> Vec<RelationGrabber> {
        self.fields
            .iter()
            .filter(|f| f.is_relation)
            .map(|f| {
                f.serialize_relation(
                    <Option<syn::Expr> as Clone>::clone(&self.diesel_table).unwrap(),
                )
            })
            .collect()
    }

    pub fn to_ast(model: ModelDecl) -> TokenStream {
        let model_name = &model.name;
        let serialized_model_name = Ident::new(
            &format!("{}{}", "Outer", model_name),
            Span::call_site().into(),
        );

        let original_fields = &model.get_fields();
        let serialized_fields = &model.get_serialized_fields();

        // TODO: don't assume half of these are applicable
        // diesel is installed here, but incase we use things that don't,
        // this will permanently error in all those other projects

        // TODO 2: when diesel is present, allow the model to have a
        // table attribute that sets applicable attributes
        let is_serde_present = crate_name("serde_json");
        let serde_check = if is_serde_present.is_ok() {
            quote! { #[derive(serde::Serialize)] }
        } else {
            quote! {}
        };

        let is_diesel_present = crate_name("diesel");
        let diesel_table = if is_diesel_present.is_ok() {
            let table = &model.clone().diesel_table.unwrap();
            quote! {
                #[derive(diesel::Queryable, diesel::Selectable, diesel::Identifiable, essence::Proxyable)]
                #[diesel(table_name = #table)]
            }
        } else {
            quote! {}
        };

        let trait_name = Ident::new(
            &format!("{}{}", "Is", model_name),
            Span::call_site().into(),
        );

        let actual_attributes: Vec<Attribute> = model
            .clone()
            .meta
            .iter()
            .filter(|a| a.path().get_ident().unwrap() != "table")
            .map(|a| a.clone())
            .collect();

        let serializer = &model.get_serializer();
        let ast = quote! {
            pub trait #trait_name {}

            #[derive(bevy_reflect::Reflect, Debug, Clone)]
            #diesel_table
            #serde_check
            #(#actual_attributes)*
            pub struct #model_name {
                #(#original_fields,)*
            }

            impl #trait_name for #model_name {}

            #[derive(Debug, Clone)]
            #serde_check
            pub struct #serialized_model_name {
                // maybe one day
                //pub __allow_safe: bool,
                #[serde(flatten)]
                pub base: #model_name,
                #(#serialized_fields,)*
            }

            impl #trait_name for #serialized_model_name {}

            #serializer
        };

        ast.into()
    }

    pub fn get_serializer(&self) -> proc_macro2::TokenStream {
        let has_relations = &self.fields.iter().find(|f| f.is_relation);
        let model_name = &self.name;
        let serialized_model_name = Ident::new(
            &format!("{}{}", "Outer", model_name),
            Span::call_site().into(),
        );

        if has_relations.is_some() {
            let relation_fields = &self.get_relation_fields();
            let relation_grabbers = &self.get_relation_grabbers();

            quote! {
                impl essence::OuterRelation for #model_name {
                    type Output = #serialized_model_name;

                    /*fn serialize(&self) -> #serialized_model_name {
                        self.serialize(false)
                    }*/

                    fn serialize(&self, connection: &mut diesel::r2d2::PooledConnection<diesel::r2d2::ConnectionManager<diesel::PgConnection>>) -> #serialized_model_name {
                        use diesel::{ RunQueryDsl, QueryDsl, BelongingToDsl, JoinOnDsl, SelectableHelper, ExpressionMethods, OptionalExtension };
                        #(#relation_grabbers)*

                        #serialized_model_name {
                            base: self.clone(),
                            #(#relation_fields,)*
                        }
                    }
                }
            }
        } else {
            quote! {
                impl essence::Outer for #model_name {
                    type Output = #serialized_model_name;

                    /*fn serialize(&self) -> #serialized_model_name {
                        self.serialize(false)
                    }*/
                    
                    fn serialize(&self) -> #serialized_model_name {
                        #serialized_model_name {
                            base: self.clone()
                        }
                    }
                }
            }
        }
    }
}

#[derive(Debug, Clone)]
struct FieldDef {
    pub meta: Vec<Attribute>,
    pub name: Ident,
    pub is_ignored: bool,
    pub is_optionally_ignored: bool,
    pub typ: TypePath,
    pub is_relation: bool,
    pub relation_data: Option<RelationData>,
}

impl FieldDef {
    fn serialize_relation(&self, own_table: Expr) -> RelationGrabber {
        let relation_data = self.relation_data.as_ref().unwrap();
        let data_clone = relation_data.clone();
        let relation_variable = Ident::new(
            format!("_{}_relation", self.name.clone()).as_str(),
            Span::call_site().into(),
        );
        let real_relation_variable = Ident::new(
            format!("{}_relation", self.name.clone()).as_str(),
            Span::call_site().into(),
        );

        let table = data_clone.table.unwrap();
        let model = data_clone.model.unwrap();

        match relation_data.relation_type {
            RelationType::ManyToMany => {
                let join_model = data_clone.join_model.unwrap();

                RelationGrabber {
                    typ: data_clone.relation_type,
                    serialize_typ: data_clone.serialization_type,
                    field: self.clone(),
                    table,
                    join_model: Some(join_model),
                    model,
                    variable: relation_variable,
                    real_variable: real_relation_variable,
                    column: data_clone.column,
                    foreign_column: data_clone.foreign_column,
                    own_table,
                    query: data_clone.query
                }
            }

            RelationType::OneToMany => RelationGrabber {
                typ: data_clone.relation_type,
                serialize_typ: data_clone.serialization_type,
                field: self.clone(),
                table,
                join_model: None,
                model,
                variable: relation_variable,
                real_variable: real_relation_variable,
                column: data_clone.column,
                foreign_column: data_clone.foreign_column,
                own_table,
                query: data_clone.query
            },

            RelationType::OneToOne => RelationGrabber {
                typ: data_clone.relation_type,
                serialize_typ: data_clone.serialization_type,
                field: self.clone(),
                table,
                join_model: None,
                model,
                variable: relation_variable,
                real_variable: real_relation_variable,
                column: data_clone.column,
                foreign_column: data_clone.foreign_column,
                own_table,
                query: data_clone.query
            },
        }
    }
}

struct RelationGrabber {
    pub typ: RelationType,
    pub serialize_typ: SerializationType,
    pub field: FieldDef,
    pub table: TypePath,
    pub join_model: Option<TypePath>,
    pub model: TypePath,
    pub variable: Ident,
    pub real_variable: Ident,
    pub column: Option<TypePath>,
    pub foreign_column: Option<TypePath>,
    pub own_table: Expr,
    pub query: Option<Expr>
}

impl ToTokens for RelationGrabber {
    fn to_tokens(&self, tokens: &mut proc_macro2::TokenStream) {
        let table = &self.table;
        let model = &self.model;
        let relation_variable = &self.variable;
        let typ = &self.field.typ;
        let own_table = &self.own_table;
        let query = &self.query;

        let query_expr;
        if query.is_some() {
            let query = query.clone().unwrap();

            query_expr = quote! {
                . #query
            }
        } else {
            query_expr = quote! {}
        }

        match self.typ {
            RelationType::ManyToMany => {
                let join_model = &self.join_model.clone().unwrap();

                quote! {
                    let #relation_variable = #join_model::belonging_to(&self)
                        .inner_join(#table::table)
                        .select(#model::as_select())
                        #query_expr
                        .load(connection)
                        .unwrap();
                }
                .to_tokens(tokens);
            }
            RelationType::OneToMany => {
                if self.column.is_some() {
                    let column = <Option<TypePath> as Clone>::clone(&self.column).unwrap();
                    let foreign = <Option<TypePath> as Clone>::clone(&self.foreign_column).unwrap();

                    // TODO: allow implicitly utilizing whatever foreign column is set by diesel
                    // instead of explicitly requiring it to be defined
                    quote! {
                        let #relation_variable = #table::table
                            .inner_join(#own_table::table.on(#column.eq(#foreign)))
                            .select(#model::as_select())
                            #query_expr
                            .load(connection)
                            .unwrap();
                    }
                    .to_tokens(tokens);
                } else {
                    quote! {
                        let #relation_variable = #model::belonging_to(&self)
                            .select(#model::as_select())
                            #query_expr
                            .load(connection)
                            .unwrap();
                    }
                    .to_tokens(tokens);
                }
            }
            RelationType::OneToOne => {
                if self.column.is_some() {
                    let column = <Option<TypePath> as Clone>::clone(&self.column).unwrap();
                    let foreign = <Option<TypePath> as Clone>::clone(&self.foreign_column).unwrap();

                    // TODO: allow implicitly utilizing whatever foreign column is set by diesel
                    // instead of explicitly requiring it to be defined
                    quote! {
                        let #relation_variable = #table::table
                            .inner_join(#own_table::table.on(#column.eq(#foreign)))
                            .select(#model::as_select())
                            #query_expr
                            .first::<#model>(connection)
                            .optional()
                            .unwrap();
                    }
                    .to_tokens(tokens);
                } else {
                    quote! {
                        let #relation_variable = #model::belonging_to(&self)
                            .select(#model::as_select())
                            #query_expr
                            .first::<#model>(connection)
                            .optional()
                            .unwrap();
                    }
                    .to_tokens(tokens);
                }
            }
        };

        handle_relation_type(
            self.real_variable.clone(),
            relation_variable.clone(),
            typ.clone(),
            self.serialize_typ,
            self.typ,
        )
        .to_tokens(tokens);
    }
}

fn handle_relation_type(
    real: Ident,
    variable: Ident,
    ty: TypePath,
    typ: SerializationType,
    rel: RelationType,
) -> proc_macro2::TokenStream {
    let function = match typ {
        SerializationType::Deep => quote! { .serialize(connection) },
        SerializationType::Normal => quote! { .serialize() },
        SerializationType::None => quote! {},
    };

    if rel == RelationType::OneToOne {
        match ty.path.segments.first().unwrap().ident.to_string().as_str() {
            "Option" => {
                quote! {
                    let #real = #variable .map(|s| s #function);
                }
            }
    
            _ => {
                quote! {
                    let #real = #variable .unwrap() #function;
                }
            }
        }
    } else {
        if typ == SerializationType::None {
            // neccessary due to forced referencing
            quote ! { let #real = #variable; }
        } else {
            match ty.path.segments.first().unwrap().ident.to_string().as_str() {
                "Vec" => {
                    quote! {
                        let #real = #variable.iter()
                            .map(|s| s #function)
                            .collect();
                    }
                }
        
                "Option" => {
                    quote! {
                        let #real = match #variable {
                            Some(s) => Some(s #function),
                            None => None
                        };
                    }
                }
        
                _ => {
                    quote! {
                        let #real = #variable #function;
                    }
                }
            }
        }
    }
}

impl Parse for FieldDef {
    #[inline]
    fn parse(input: ParseStream) -> Result<FieldDef, syn::Error> {
        let meta = syn::Attribute::parse_outer(input)?;
        let name = input.parse::<Ident>()?;
        let _arrow = input.parse::<Token![->]>()?;
        let typ = input.parse::<TypePath>()?;

        let is_ignored = match &meta.iter() {
            v if v
                .clone()
                .find(|attr| match &attr.meta {
                    Meta::Path(path) => path.get_ident().unwrap() == "ignored",
                    _ => false,
                })
                .is_some() =>
            {
                true
            }
            _ => false,
        };

        let is_optionally_ignored = match &meta.iter() {
            v if v
                .clone()
                .find(|attr| match &attr.meta {
                    Meta::Path(path) => path.get_ident().unwrap() == "optionally_ignored",
                    _ => false,
                })
                .is_some() =>
            {
                true
            }
            _ => false,
        };

        let mut relation_data = None;
        let binding = meta.clone();
        let relation_attr = binding.iter().find(|attr| match &attr.meta {
            Meta::List(list) => list.path.get_ident().unwrap() == "relation",
            _ => false,
        });

        if relation_attr.is_some() {
            relation_data = Some(relation_attr.unwrap().parse_args().unwrap());
        }

        Ok(FieldDef {
            meta,
            name,
            typ,
            is_ignored,
            is_optionally_ignored,
            is_relation: relation_attr.is_some(),
            relation_data: relation_data,
        })
    }
}

impl ToTokens for FieldDef {
    fn to_tokens(&self, tokens: &mut proc_macro2::TokenStream) {
        if self.is_ignored {
            quote! { #[serde(skip_serializing)] }.to_tokens(tokens);
        }

        let unwanted_attr = vec!["ignored","relation"];
        let actual_attributes = self.meta.iter()
            .filter(|f| !unwanted_attr.iter().any(|ff| f.path().get_ident().unwrap() == ff));

        quote! {
            #(#actual_attributes)*
        }.to_tokens(tokens);

        tokens.append(Ident::new("pub", Span::call_site().into()));
        tokens.append(self.name.clone());
        tokens.append(Punct::new(':', proc_macro2::Spacing::Alone));
        self.typ.path.clone().to_tokens(tokens);
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
enum RelationType {
    OneToOne,
    OneToMany,
    ManyToMany,
}

#[derive(Debug, Clone, Copy, PartialEq)]
enum SerializationType {
    Deep,
    Normal,
    None,
}

#[derive(Debug, Clone)]
struct RelationData {
    pub relation_type: RelationType,
    pub serialization_type: SerializationType,
    pub model: Option<TypePath>,
    pub join_model: Option<TypePath>,
    pub table: Option<TypePath>,
    pub column: Option<TypePath>,
    pub foreign_column: Option<TypePath>,
    pub query: Option<Expr>
}

impl Parse for RelationData {
    #[inline]
    fn parse(input: ParseStream) -> Result<RelationData, syn::Error> {
        let mut base = RelationData {
            relation_type: RelationType::OneToMany,
            serialization_type: SerializationType::None,
            model: None,
            join_model: None,
            table: None,
            column: None,
            foreign_column: None,
            query: None
        };

        loop {
            let key_ident = input.parse::<Ident>();

            if key_ident.is_err() {
                break;
            }

            let key = key_ident.unwrap();
            let _eq = input.parse::<Token![=]>()?;

            match key.to_string().as_str() {
                "typ" => {
                    let rl_type = match input.parse::<Ident>()?.to_string().as_str() {
                        "ManyToMany" => RelationType::ManyToMany,
                        "OneToMany" => RelationType::OneToMany,
                        "OneToOne" => RelationType::OneToOne,
                        _ => panic!("Unknown relation type provided"),
                    };

                    base.relation_type = rl_type;
                }

                "serialize" => {
                    let ser_type = match input.parse::<Ident>()?.to_string().as_str() {
                        "Deep" => SerializationType::Deep,
                        "Normal" => SerializationType::Normal,
                        _ => SerializationType::None,
                    };

                    base.serialization_type = ser_type;
                }

                "model" => base.model = Some(input.parse::<TypePath>()?),

                "join_model" => base.join_model = Some(input.parse::<TypePath>()?),

                "table" => base.table = Some(input.parse::<TypePath>()?),

                "column" => base.column = Some(input.parse::<TypePath>()?),

                "foreign" => base.foreign_column = Some(input.parse::<TypePath>()?),

                "foreign_column" => base.foreign_column = Some(input.parse::<TypePath>()?),

                "query" => base.query = Some(input.parse::<Expr>()?),

                // necessary as &str is exhaustive
                _ => (),
            }

            let _delimiter = input.parse::<Token![,]>();

            if _delimiter.is_err() {
                break;
            }
        }

        Ok(base)
    }
}

struct RelationFieldDef {
    pub field: FieldDef,
}

impl ToTokens for RelationFieldDef {
    fn to_tokens(&self, tokens: &mut proc_macro2::TokenStream) {
        tokens.append(self.field.name.clone());
        tokens.append(Punct::new(':', proc_macro2::Spacing::Alone));
        tokens.append(Ident::new(
            &format!("{}_relation", self.field.name.to_string()),
            Span::call_site().into(),
        ));
    }
}

#[proc_macro]
pub fn model(input: TokenStream) -> TokenStream {
    match syn::parse(input) {
        Ok(input) => ModelDecl::to_ast(input).into(),
        Err(_) => panic!("An invalid syntax was provided to the essence model parser."),
    }
}

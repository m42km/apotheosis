SELECT * FROM "challenges"
WHERE NOT "challenges"."flags" & 1 = 0 /* 1 is the current flag for hidden challenges */
OFFSET $1
LIMIT $2;
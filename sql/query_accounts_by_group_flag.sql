SELECT "accounts".* FROM "accounts"
LEFT OUTER JOIN "group_assignments"
    ON "accounts"."id" = "group_assignments"."account_id"
LEFT OUTER JOIN "groups"
    ON "group_assignments"."group_id" = "groups"."id"
WHERE NOT "groups"."flags" & $1 = 0 OR NOT "groups"."flags" & $2 = 0 OR NOT "groups"."flags" & $3 = 0;